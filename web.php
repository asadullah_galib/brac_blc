<?php

Route::get('/locale/{lang}', function($lang) {
	Session::put('locale', $lang);
	return redirect()->back();
})->name('locale');

Route::get('/',function(){
	return redirect()->route('login');
});

//Reset Password
Route::get('reset/password','Backend\PasswordResetController@resetPassword')->name('reset.password');
Route::post('check/email','Backend\PasswordResetController@checkEmail')->name('check.email');
Route::get('check/name','Backend\PasswordResetController@checkName')->name('check.name');
Route::get('check/code','Backend\PasswordResetController@checkCode')->name('check.code');
Route::post('submit/check/code','Backend\PasswordResetController@submitCode')->name('submit.check.code');
Route::get('new/password','Backend\PasswordResetController@newPassword')->name('new.password');
Route::post('store/new/password','Backend\PasswordResetController@newPasswordStore')->name('store.new.password');



Auth::routes();

Route::middleware(['auth'])->group(function(){

	Route::get('/home', 'Backend\HomeController@index')->name('dashboard');	

	Route::group(['middleware'=>['permission']],function(){

		Route::prefix('menu')->group(function(){
			Route::get('/view', 'Backend\Menu\MenuController@index')->name('menu');
			Route::get('/add', 'Backend\Menu\MenuController@add')->name('menu.add');
			Route::post('/store', 'Backend\Menu\MenuController@store')->name('menu.store');
			Route::get('/edit/{id}', 'Backend\Menu\MenuController@edit')->name('menu.edit');
			Route::post('/update/{id}','Backend\Menu\MenuController@update')->name('menu.update');
			Route::get('/subparent','Backend\Menu\MenuController@getSubParent')->name('menu.getajaxsubparent');

			Route::get('/icon','Backend\Menu\MenuIconController@index')->name('menu.icon');
			Route::post('icon/store','Backend\Menu\MenuIconController@store')->name('menu.icon.store');
			Route::get('icon/edit','Backend\Menu\MenuIconController@edit')->name('menu.icon.edit');
			Route::post('icon/update/{id}','Backend\Menu\MenuIconController@update')->name('menu.icon.update');
			Route::post('icon/delete','Backend\Menu\MenuIconController@delete')->name('menu.icon.delete');
		});



		Route::prefix('user')->group(function(){
			Route::get('/','UserController@index')->name('user');
			Route::post('/store','UserController@storeUser')->name('user.store');
			Route::get('/edit/{id}','UserController@getUser')->name('user.edit');
			Route::post('/update','UserController@updateUser')->name('user.update');
			Route::post('/delete','UserController@deleteUser')->name('user.delete');

			Route::get('/role','Backend\Menu\RoleController@index')->name('user.role');
			Route::post('/role/store','Backend\Menu\RoleController@storeRole')->name('user.role.store');
			Route::get('/role/edit','Backend\Menu\RoleController@getRole')->name('user.role.edit');
			Route::post('/role/update/{id}','Backend\Menu\RoleController@updateRole')->name('user.role.update');
			Route::post('/role/delete','Backend\Menu\RoleController@deleteRole')->name('user.role.delete');

			Route::get('/permission','Backend\Menu\MenuPermissionController@index')->name('user.permission');
			Route::get('/permission/store','Backend\Menu\MenuPermissionController@storePermission')->name('user.permission.store');
		});

		Route::prefix('site')->group(function(){
			Route::get('settings','Backend\SiteSettingController@index')->name('site.setting');		
			Route::post('settings/update','Backend\SiteSettingController@updateSetting')->name('site.setting.update');	
			Route::get('slider','Backend\SliderController@index')->name('site.slider');
			Route::get('slider/add','Backend\SliderController@addSlider')->name('site.slider.add');
			Route::post('slider/store','Backend\SliderController@storeSlider')->name('site.slider.store');
		});

		Route::prefix('tour-package')->group(function(){
			Route::get('view','Backend\SiteSettingController@index')->name('tour-package.view');
		});
		Route::prefix('setup-management')->group(function(){
			//Resource Person
			Route::get('resource-person/view','Backend\ResourcePersonController@index')->name('setup-management.resource.view');
			Route::get('resource-person/add','Backend\ResourcePersonController@addResource')->name('setup-management.resource.add');	
			Route::post('resource-person/store','Backend\ResourcePersonController@storeResource')->name('setup-management.resource.store');
			Route::get('resource-person/edit/{id}','Backend\ResourcePersonController@editResource')->name('setup-management.resource.edit');
			Route::post('resource-person/update/{id}','Backend\ResourcePersonController@updateResource')->name('setup-management.resource.update');
			Route::get('resource-person/delete','Backend\ResourcePersonController@deleteResource')->name('setup-management.resource.delete');

			// Client route
			Route::get('client/view','Backend\ClientController@index')->name('setup-management.client.view');
			Route::get('client/add','Backend\ClientController@addClient')->name('setup-management.client.add');	
			Route::post('client/store','Backend\ClientController@storeClient')->name('setup-management.client.store');
			Route::get('client/edit/{id}','Backend\ClientController@editClient')->name('setup-management.client.edit');
			Route::post('client/update/{id}','Backend\ClientController@updateClient')->name('setup-management.client.update');
			Route::get('client/delete','Backend\ClientController@deleteClient')->name('setup-management.client.delete');

			// Project routes	
			Route::get('project/view','Backend\ProjectController@index')->name('setup-management.project.view');
			Route::get('project/add','Backend\ProjectController@addProject')->name('setup-management.project.add');	
			Route::post('project/store','Backend\ProjectController@storeProject')->name('setup-management.project.store');
			Route::get('project/edit/{id}','Backend\ProjectController@editProject')->name('setup-management.project.edit');
			Route::post('project/update/{id}','Backend\ProjectController@updateProject')->name('setup-management.project.update');
			Route::get('project/delete','Backend\ProjectController@deleteProject')->name('setup-management.project.delete');

			// Contact person
			Route::get('contact-person/view','Backend\ContactPersonController@index')->name('setup-management.contact.view');
			Route::get('contact-person/add','Backend\ContactPersonController@addContact')->name('setup-management.contact.add');	
			Route::post('contact-person/store','Backend\ContactPersonController@storeContact')->name('setup-management.contact.store');
			Route::get('contact-person/edit/{id}','Backend\ContactPersonController@editContact')->name('setup-management.contact.edit');
			Route::post('contact-person/update/{id}','Backend\ContactPersonController@updateContact')->name('setup-management.contact.update');
			Route::get('contact-person/delete','Backend\ContactPersonController@deleteContact')->name('setup-management.contact.delete');

			//Role
			Route::get('role/view','Backend\RoleController@index')->name('setup-management.role.view');
			Route::get('role/add','Backend\RoleController@addRole')->name('setup-management.role.add');	
			Route::post('role/store','Backend\RoleController@storeRole')->name('setup-management.role.store');
			Route::get('role/edit/{id}','Backend\RoleController@editRole')->name('setup-management.role.edit');
			Route::post('role/update/{id}','Backend\RoleController@updateRole')->name('setup-management.role.update');
			Route::get('role/delete','Backend\RoleController@deleteRole')->name('setup-management.role.delete');

			//Resource Person use
			Route::get('resources-use/view','Backend\ResourcePersonProjectController@index')->name('setup-management.person.project.view');
			Route::get('resources-use/add','Backend\ResourcePersonProjectController@addPersonProject')->name('setup-management.person.project.add');	
			Route::post('resources-use/store','Backend\ResourcePersonProjectController@storePersonProject')->name('setup-management.person.project.store');
			Route::get('resources-use/edit/{id}','Backend\ResourcePersonProjectController@editPersonProject')->name('setup-management.person.project.edit');
			Route::post('resources-use/update/{id}','Backend\ResourcePersonProjectController@updatePersonProject')->name('setup-management.person.project.update');
			Route::get('resources-use/delete','Backend\ResourcePersonProjectController@deletePersonProject')->name('setup-management.person.project.delete');
		});

		Route::prefix('project-management')->group(function(){


// Resource Contribution
			Route::get('resource-contribution/view','Backend\ResourceContributionController@index')->name('project-management.contribution.view');
			Route::get('resource-contribution/add','Backend\ResourceContributionController@addContribution')->name('project-management.contribution.add');	
			Route::post('resource-contribution/store','Backend\ResourceContributionController@storeContribution')->name('project-management.contribution.store');
			Route::get('resource-contribution/edit/{id}','Backend\ResourceContributionController@editContribution')->name('project-management.contribution.edit');
			Route::post('resource-contribution/update/{id}','Backend\ResourceContributionController@updateContribution')->name('project-management.contribution.update');
			Route::get('resource-contribution/delete','Backend\ResourceContributionController@deleteContribution')->name('project-management.contribution.delete');

//Evaluation
			Route::get('evaluation/view','Backend\EvaluationController@index')->name('project-management.evaluation.view');
			Route::get('evaluation/add','Backend\EvaluationController@addEvaluation')->name('project-management.evaluation.add');	
			Route::post('evaluation/store','Backend\EvaluationController@storeEvaluation')->name('project-management.evaluation.store');
			Route::get('evaluation/edit/{id}','Backend\EvaluationController@editEvaluation')->name('project-management.evaluation.edit');
			Route::get('evaluation/form/view/{id}','Backend\EvaluationController@viewEvaluation')->name('project-management.evaluation.form.view');
			Route::post('evaluation/update/{id}','Backend\EvaluationController@updateEvaluation')->name('project-management.evaluation.update');
			Route::get('evaluation/delete','Backend\EvaluationController@deleteEvaluation')->name('project-management.evaluation.delete');
			Route::get('own/evaluation/','Backend\EvaluationController@ownEvaluation')->name('project-management.own.evaluation');
//Task Assign
			Route::get('task-assign/view','Backend\TaskAssignController@index')->name('project-management.task.assign.view');
			Route::get('task-assign/add','Backend\TaskAssignController@addTaskAssign')->name('project-management.task.assign.add');	
			Route::post('task-assign/store','Backend\TaskAssignController@storeTaskAssign')->name('project-management.task.assign.store');
			Route::get('task-assign/edit/{id}','Backend\TaskAssignController@editTaskAssign')->name('project-management.task.assign.edit');
			Route::post('task-assign/update/{id}','Backend\TaskAssignController@updateTaskAssign')->name('project-management.task.assign.update');
			Route::get('task-assign/delete','Backend\TaskAssignController@deleteTaskAssign')->name('project-management.task.assign.delete');
// Meeting
			Route::get('meeting/view','Backend\MeetingController@index')->name('project-management.meeting.view');
			Route::get('meeting/add','Backend\MeetingController@addMeeting')->name('project-management.meeting.add');	
			Route::post('meeting/store','Backend\MeetingController@storeMeeting')->name('project-management.meeting.store');
			Route::get('meeting/edit/{id}','Backend\MeetingController@editMeeting')->name('project-management.meeting.edit');
			Route::post('meeting/update/{id}','Backend\MeetingController@updateMeeting')->name('project-management.meeting.update');
			Route::get('meeting/delete','Backend\MeetingController@deleteMeeting')->name('project-management.meeting.delete');

// Profile
			Route::get('resource/profile/view','Backend\ResourceProfileController@index')->name('project-management.resource.profile.view');
			Route::get('resource/profile/edit/{id}','Backend\ResourceProfileController@editProfile')->name('project-management.resource.profile.edit');
			Route::post('resource/profile/update/{id}','Backend\ResourceProfileController@updateProfile')->name('project-management.resource.profile.update');

// All resourcess view
			Route::get('all/resource/view','Backend\ResourcePersonController@allResource')->name('project-management.all.resource.view');

//Change Password
			Route::get('change/password','Backend\PasswordChangeController@changePassword')->name('project-management.change.password');
			Route::post('store/password','Backend\PasswordChangeController@storePassword')->name('project-management.store.password');

//Report
			Route::get('man_month/report','Backend\ReportController@manMonth')->name('project-management.man_month.report');
			Route::get('individual_man_month/report/{id}','Backend\ReportController@individualManMonth')->name('project-management.individual_man_month.report');
			Route::get('individual_man_month/details/report','Backend\ReportController@individualManMonthDetails')->name('project-management.individual_man_month.details.report');

//Attendance
			Route::get('attendance/resource_person','Backend\AttendanceController@resource')->name('project-management.attendance.resource');
			Route::get('individual/attendance/resource_person/{date?}/{resource_person?}','Backend\AttendanceController@individualAttendance')->name('project-management.individual.attendance.resource');
			

		});
});
});
