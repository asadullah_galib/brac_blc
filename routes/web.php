<?php

Route::get('/locale/{lang}', function($lang) {
	Session::put('locale', $lang);
	return redirect()->back();
})->name('locale');

Route::get('/','Frontend\FrontController@index')->name('frontend.home');
Route::get('/about-us','Frontend\FrontController@aboutus')->name('frontend.aboutus');
Route::get('/service','Frontend\FrontController@service')->name('frontend.service');
Route::get('/gallery','Frontend\FrontController@gallery')->name('frontend.gallery');
Route::get('/booking-system','Frontend\FrontController@bookingSystem')->name('frontend.booking.system');
Route::get('/faq','Frontend\FrontController@faq')->name('frontend.faq');
Route::get('/user-login','Frontend\FrontController@useLogin')->name('frontend.login');
Route::get('/user-signup','Frontend\FrontController@userSignup')->name('frontend.signup');
Route::get('/forgot-password','Frontend\FrontController@forgotPassword')->name('frontend.forgot.password');
Route::get('/blog','Frontend\FrontController@blog')->name('frontend.blog');
Route::get('/contact-us','Frontend\FrontController@contactus')->name('frontend.contactus');
Route::get('/user-dashboard','Frontend\FrontController@userDashboard')->name('frontend.user.dashboard');
Route::get('/user-edit-into','Frontend\FrontController@userEditInfo')->name('frontend.user.edit.info');
Route::get('/user-edit-details','Frontend\FrontController@userEditDetails')->name('frontend.user.edit.details');

//Reset Password
Route::get('reset/password','Backend\PasswordResetController@resetPassword')->name('reset.password');
Route::post('check/email','Backend\PasswordResetController@checkEmail')->name('check.email');
Route::get('check/name','Backend\PasswordResetController@checkName')->name('check.name');
Route::get('check/code','Backend\PasswordResetController@checkCode')->name('check.code');
Route::post('submit/check/code','Backend\PasswordResetController@submitCode')->name('submit.check.code');
Route::get('new/password','Backend\PasswordResetController@newPassword')->name('new.password');
Route::post('store/new/password','Backend\PasswordResetController@newPasswordStore')->name('store.new.password');

//change Password
Route::get('change/password','Backend\PasswordChangeController@changePassword')->name('change.password');
Route::post('store/password','Backend\PasswordChangeController@storePassword')->name('store.password');

Auth::routes();

Route::middleware(['auth'])->group(function(){

	Route::get('/home', 'Backend\HomeController@index')->name('dashboard');	

	Route::group(['middleware'=>['permission']],function(){

		Route::prefix('menu')->group(function(){
			Route::get('/view', 'Backend\Menu\MenuController@index')->name('menu');
			Route::get('/add', 'Backend\Menu\MenuController@add')->name('menu.add');
			Route::post('/store', 'Backend\Menu\MenuController@store')->name('menu.store');
			Route::get('/edit/{id}', 'Backend\Menu\MenuController@edit')->name('menu.edit');
			Route::post('/update/{id}','Backend\Menu\MenuController@update')->name('menu.update');
			Route::get('/subparent','Backend\Menu\MenuController@getSubParent')->name('menu.getajaxsubparent');
			Route::get('/icon','Backend\Menu\MenuIconController@index')->name('menu.icon');
			Route::post('icon/store','Backend\Menu\MenuIconController@store')->name('menu.icon.store');
			Route::get('icon/edit','Backend\Menu\MenuIconController@edit')->name('menu.icon.edit');
			Route::post('icon/update/{id}','Backend\Menu\MenuIconController@update')->name('menu.icon.update');
			Route::post('icon/delete','Backend\Menu\MenuIconController@delete')->name('menu.icon.delete');
		});

		Route::prefix('user')->group(function(){
			//User
			Route::get('/view','Backend\Menu\UserController@userView')->name('user.view');
			Route::get('/add','Backend\Menu\UserController@userAdd')->name('user.add');
			Route::post('/store','Backend\Menu\UserController@userStore')->name('user.store');
			Route::get('/edit/{id}','Backend\Menu\UserController@userEdit')->name('user.edit');
			Route::post('/update/{id}','Backend\Menu\UserController@userUpdate')->name('user.update');
			Route::post('/delete','Backend\Menu\UserController@userDelete')->name('user.delete');
			Route::get('/get-user-role','Backend\Menu\UserController@getUserRole')->name('get-user-role');
			//BLC Base User
			Route::get('/blc/view','Backend\Menu\UserController@blcUserView')->name('user.blc.view');
			Route::get('/blc/add','Backend\Menu\UserController@blcUserAdd')->name('user.blc.add');
			Route::post('/blc/store','Backend\Menu\UserController@blcUserStore')->name('user.blc.store');
			Route::get('/blc/edit/{id}','Backend\Menu\UserController@blcUserEdit')->name('user.blc.edit');
			Route::post('/blc/update/{id}','Backend\Menu\UserController@blcUserUpdate')->name('user.blc.update');
			Route::post('/blc/delete','Backend\Menu\UserController@blcUserDelete')->name('user.blc.delete');
			//User Role
			Route::get('/role/view','Backend\Menu\RoleController@roleView')->name('user.role.view');
			Route::get('/role/add','Backend\Menu\RoleController@roleAdd')->name('user.role.add');
			Route::post('/role/store','Backend\Menu\RoleController@roleStore')->name('user.role.store');
			Route::get('/role/edit/{id}','Backend\Menu\RoleController@roleEdit')->name('user.role.edit');
			Route::post('/role/update/{id}','Backend\Menu\RoleController@roleUpdate')->name('user.role.update');
			Route::post('/role/delete/{id}','Backend\Menu\RoleController@storeDelete')->name('user.role.destroy');
			//User Role Type
			Route::get('/role/type/view','Backend\Menu\RoleTypeController@roleTypeView')->name('user.role.type.view');
			Route::get('/role/type/add','Backend\Menu\RoleTypeController@roleTypeAdd')->name('user.role.type.add');
			Route::post('/role/type/store','Backend\Menu\RoleTypeController@roleTypeStore')->name('user.role.type.store');
			Route::get('/role/type/edit/{id}','Backend\Menu\RoleTypeController@roleTypeEdit')->name('user.role.type.edit');
			Route::post('/role/type/update/{id}','Backend\Menu\RoleTypeController@roleTypeUpdate')->name('user.role.type.update');
			Route::post('/role/type/delete','Backend\Menu\RoleTypeController@roleTypeDelete')->name('user.role.type.destory');
			//Menu Permission
			Route::get('/permission','Backend\Menu\MenuPermissionController@index')->name('user.permission');
			Route::get('/permission/store','Backend\Menu\MenuPermissionController@storePermission')->name('user.permission.store');
		});

		Route::prefix('setup')->group(function(){
			//BLC Category
			Route::get('/blc/category/view','Backend\Setup\BlcCategoryController@blcCategoryView')->name('setup.blc.category.view');
			Route::get('/blc/category/add','Backend\Setup\BlcCategoryController@blcCategoryAdd')->name('setup.blc.category.add');
			Route::post('/blc/category/store','Backend\Setup\BlcCategoryController@blcCategoryStore')->name('setup.blc.category.store');
			Route::get('/blc/category/eidt/{id}','Backend\Setup\BlcCategoryController@blcCategoryEdit')->name('setup.blc.category.edit');
			Route::post('/blc/category/update/{id}','Backend\Setup\BlcCategoryController@blcCategoryUpdate')->name('setup.blc.category.update');
			Route::post('/blc/category/delete/{id}','Backend\Setup\BlcCategoryController@blcCategoryDelete')->name('setup.blc.category.destroy');
			//BLC Name
			Route::get('/blc/name/view','Backend\Setup\BlcNameController@blcNameView')->name('setup.blc.name.view');
			Route::get('/blc/name/add','Backend\Setup\BlcNameController@blcNameAdd')->name('setup.blc.name.add');
			Route::post('/blc/name/store','Backend\Setup\BlcNameController@blcNameStore')->name('setup.blc.name.store');
			Route::get('/blc/name/eidt/{id}','Backend\Setup\BlcNameController@blcNameEdit')->name('setup.blc.name.edit');
			Route::post('/blc/name/update/{id}','Backend\Setup\BlcNameController@blcNameUpdate')->name('setup.blc.name.update');
			Route::post('/blc/name/delete/{id}','Backend\Setup\BlcNameController@blcNameDelete')->name('setup.blc.name.destroy');
			//BLC Room-Category
			Route::get('/blc/room-category/view','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryView')->name('setup.blc.room-category.view');
			Route::get('/blc/room-category/add','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryAdd')->name('setup.blc.room-category.add');
			Route::post('/blc/room-category/store','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryStore')->name('setup.blc.room-category.store');
			Route::get('/blc/room-category/eidt/{id}','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryEdit')->name('setup.blc.room-category.edit');
			Route::post('/blc/room-category/update/{id}','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryUpdate')->name('setup.blc.room-category.update');
			Route::post('/blc/room-category/delete/{id}','Backend\Setup\BlcRoomCategoryController@blcRoomCategoryDelete')->name('setup.blc.room-category.destroy');
			//BLC Room-Facilities
			Route::get('/blc/room-facilities/view','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityView')->name('setup.blc.room-facilities.view');
			Route::get('/blc/room-facilities/add','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityAdd')->name('setup.blc.room-facilities.add');
			Route::post('/blc/room-facilities/store','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityStore')->name('setup.blc.room-facilities.store');
			Route::get('/blc/room-facilities/eidt/{id}','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityEdit')->name('setup.blc.room-facilities.edit');
			Route::post('/blc/room-facilities/update/{id}','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityUpdate')->name('setup.blc.room-facilities.update');
			Route::post('/blc/room-facilities/delete/{id}','Backend\Setup\BlcRoomFacilityController@blcRoomFacilityDelete')->name('setup.blc.room-facilities.destroy');
			//BLC Room-Type
			Route::get('/blc/room-type/view','Backend\Setup\BlcRoomTypeController@blcRoomTypeView')->name('setup.blc.room-type.view');
			Route::get('/blc/room-type/add','Backend\Setup\BlcRoomTypeController@blcRoomTypeAdd')->name('setup.blc.room-type.add');
			Route::post('/blc/room-type/store','Backend\Setup\BlcRoomTypeController@blcRoomTypeStore')->name('setup.blc.room-type.store');
			Route::get('/blc/room-type/eidt/{id}','Backend\Setup\BlcRoomTypeController@blcRoomTypeEdit')->name('setup.blc.room-type.edit');
			Route::post('/blc/room-type/update/{id}','Backend\Setup\BlcRoomTypeController@blcRoomTypeUpdate')->name('setup.blc.room-type.update');
			Route::post('/blc/room-type/delete/{id}','Backend\Setup\BlcRoomTypeController@blcRoomTypeDelete')->name('setup.blc.room-type.destroy');
		});
	});
});
