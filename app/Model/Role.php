<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\UserRoleType;

class Role extends Model
{
    //
    use SoftDeletes;
    protected $fillable = ['name','role_slug'];

    public function user_role_type(){
    	return $this->belongsTo(UserRoleType::class,'user_role_type_id','id');
    }
}
