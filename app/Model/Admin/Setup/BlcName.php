<?php

namespace App\Model\Admin\Setup;

use Illuminate\Database\Eloquent\Model;

class BlcName extends Model
{
    public function blc_category(){
    	return $this->belongsTo(BlcCategory::class,'blc_category_id','id');
    }
}
