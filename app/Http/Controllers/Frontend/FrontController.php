<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App;

class FrontController extends Controller
{
    public function index()
    {
    	return view('frontend.home');
    }

    public function aboutus(){
    	return view('frontend.single_pages.about-us');
    }

    public function service(){
    	return view('frontend.single_pages.service');
    }

    public function gallery(){
    	return view('frontend.single_pages.gallery');
    }

    public function bookingSystem(){
    	return view('frontend.single_pages.booking-system');
    }

    public function faq(){
    	return view('frontend.single_pages.faq');
    }

    public function useLogin(){
    	return view('frontend.single_pages.user-login');
    }

    public function userSignup(){
    	return view('frontend.single_pages.user-signup');
    }

    public function forgotPassword(){
    	return view('frontend.single_pages.forgot-password');
    }

    public function blog(){
    	return view('frontend.single_pages.blog');
    }

    public function contactus(){
    	return view('frontend.single_pages.contact-us');
    }

    public function userDashboard(){
        return view('frontend.single_pages.user-dashboard');
    }

    public function userEditInfo(){
        return view('frontend.single_pages.edit-user-info');
    }

    public function userEditDetails(){
        return view('frontend.single_pages.edit-user-details');
    }
}
