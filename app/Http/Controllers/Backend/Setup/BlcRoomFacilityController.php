<?php

namespace App\Http\Controllers\Backend\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Setup\BlcCategory;
use App\Model\Admin\Setup\BlcName;
use App\Model\Admin\Setup\RoomCategory;
use App\Model\Admin\Setup\RoomFacility;
use App\Model\Admin\Setup\RoomType;
use Auth;
use App\Http\Requests\BlcRoomFacilityRequest;
use DB;

class BlcRoomFacilityController extends Controller
{
    public function blcRoomFacilityView(){
    	$allData = RoomFacility::where('status','1')->get();
    	return view('backend.setup.room_facility.view-room-facility',compact('allData'));
    }

    public function blcRoomFacilityAdd(){
    	return view('backend.setup.room_facility.add-room-facility');
    }

    public function blcRoomFacilityStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:room_facilities,name'
        ]);
    	$rmfacility = new RoomFacility();
    	$rmfacility->name = $request->name;
    	$rmfacility->created_by = Auth::user()->save();
        if ($request->file('image')){
            $file = $request->file('image');
            $filename =date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/room_facility_icon'), $filename);
            $rmfacility['image']= $filename;
        }
    	$rmfacility->save();
    	return redirect()->route('setup.blc.room-facilities.view')->with('success','Data Saved Successfully');
    }

    public function blcRoomFacilityEdit($id){
    	$data['editData'] = RoomFacility::find($id);
    	return view('backend.setup.room_facility.add-room-facility',$data);
    }

    public function blcRoomFacilityUpdate(BlcRoomFacilityRequest $request,$id){
    	$rmfacility = RoomFacility::find($id);
    	$rmfacility->name = $request->name;
    	$rmfacility->updated_by = Auth::user()->save();
        if ($request->file('image')){
            $file = $request->file('image');
            @unlink(public_path('upload/room_facility_icon/'.$rmfacility->image));
            $filename =date('YmdHi').$file->getClientOriginalName();
            $file->move(public_path('upload/room_facility_icon'), $filename);
            $rmfacility['image']= $filename;
        }
    	$rmfacility->save();
    	return redirect()->route('setup.blc.room-facilities.view')->with('success','Data Updated Successfully');
    }

    public function blcRoomFacilityDelete($id){
    	
    }
}
