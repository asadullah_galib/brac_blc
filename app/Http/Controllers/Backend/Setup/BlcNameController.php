<?php

namespace App\Http\Controllers\Backend\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Setup\BlcCategory;
use App\Model\Admin\Setup\BlcName;
use App\Model\Admin\Setup\RoomCategory;
use App\Model\Admin\Setup\RoomFacility;
use App\Model\Admin\Setup\RoomType;
use Auth;
use App\Http\Requests\BlcNameRequest;
use DB;

class BlcNameController extends Controller
{
    public function blcNameView(){
    	$allData = BlcName::where('status','1')->get();
    	return view('backend.setup.blc_name.view-blc-name',compact('allData'));
    }

    public function blcNameAdd(){
        $data['blcCategories'] = BlcCategory::where('status','1')->get();
    	return view('backend.setup.blc_name.add-blc-name',$data);
    }

    public function blcNameStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:blc_names,name'
        ]);
    	$blcname = new BlcName();
    	$blcname->name = $request->name;
        $blcname->blc_category_id = $request->blc_category_id;
    	$blcname->created_by = Auth::user()->save();
    	$blcname->save();
    	return redirect()->route('setup.blc.name.view')->with('success','Data Saved Successfully');
    }

    public function blcNameEdit($id){
        $data['blcCategories'] = BlcCategory::where('status','1')->get();
    	$data['editData'] = BlcName::find($id);
    	return view('backend.setup.blc_name.add-blc-name',$data);
    }

    public function blcNameUpdate(BlcNameRequest $request,$id){
    	$blcname = BlcName::find($id);
    	$blcname->name = $request->name;
        $blcname->blc_category_id = $request->blc_category_id;
    	$blcname->updated_by = Auth::user()->save();
    	$blcname->save();
    	return redirect()->route('setup.blc.name.view')->with('success','Data Updated Successfully');
    }

    public function blcNameDelete($id){
    	
    }
}
