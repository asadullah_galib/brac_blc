<?php

namespace App\Http\Controllers\Backend\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Setup\BlcCategory;
use App\Model\Admin\Setup\BlcName;
use App\Model\Admin\Setup\RoomCategory;
use App\Model\Admin\Setup\RoomFacility;
use App\Model\Admin\Setup\RoomType;
use Auth;
use App\Http\Requests\BlcCategoryRequest;
use DB;

class BlcCategoryController extends Controller
{
    public function blcCategoryView(){
    	$allData = BlcCategory::where('status','1')->get();
    	return view('backend.setup.blc_category.view-blc-category',compact('allData'));
    }

    public function blcCategoryAdd(){
    	return view('backend.setup.blc_category.add-blc-category');
    }

    public function blcCategoryStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:blc_categories,name'
        ]);
    	$category = new BlcCategory();
    	$category->name = $request->name;
    	$category->created_by = Auth::user()->save();
    	$category->save();
    	return redirect()->route('setup.blc.category.view')->with('success','Data Saved Successfully');
    }

    public function blcCategoryEdit($id){
    	$data['editData'] = BlcCategory::find($id);
    	return view('backend.setup.blc_category.add-blc-category',$data);
    }

    public function blcCategoryUpdate(BlcCategoryRequest $request,$id){
    	$category = BlcCategory::find($id);
    	$category->name = $request->name;
    	$category->updated_by = Auth::user()->save();
    	$category->save();
    	return redirect()->route('setup.blc.category.view')->with('success','Data Updated Successfully');
    }

    public function blcCategoryDelete($id){
    	
    }
}
