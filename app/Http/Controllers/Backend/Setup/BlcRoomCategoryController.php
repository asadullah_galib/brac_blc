<?php

namespace App\Http\Controllers\Backend\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Setup\BlcCategory;
use App\Model\Admin\Setup\BlcName;
use App\Model\Admin\Setup\RoomCategory;
use App\Model\Admin\Setup\RoomFacility;
use App\Model\Admin\Setup\RoomType;
use Auth;
use App\Http\Requests\BlcRoomCategoryRequest;
use DB;

class BlcRoomCategoryController extends Controller
{
    public function blcRoomCategoryView(){
    	$allData = RoomCategory::where('status','1')->get();
    	return view('backend.setup.room_category.view-room-category',compact('allData'));
    }

    public function blcRoomCategoryAdd(){
    	return view('backend.setup.room_category.add-room-category');
    }

    public function blcRoomCategoryStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:room_categories,name'
        ]);
    	$rmcategory = new RoomCategory();
    	$rmcategory->name = $request->name;
    	$rmcategory->created_by = Auth::user()->save();
    	$rmcategory->save();
    	return redirect()->route('setup.blc.room-category.view')->with('success','Data Saved Successfully');
    }

    public function blcRoomCategoryEdit($id){
    	$data['editData'] = RoomCategory::find($id);
    	return view('backend.setup.room_category.add-room-category',$data);
    }

    public function blcRoomCategoryUpdate(BlcRoomCategoryRequest $request,$id){
    	$rmcategory = RoomCategory::find($id);
    	$rmcategory->name = $request->name;
    	$rmcategory->updated_by = Auth::user()->save();
    	$rmcategory->save();
    	return redirect()->route('setup.blc.room-category.view')->with('success','Data Updated Successfully');
    }

    public function blcRoomCategoryDelete($id){
    	
    }
}
