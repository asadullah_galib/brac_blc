<?php

namespace App\Http\Controllers\Backend\Setup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Admin\Setup\BlcCategory;
use App\Model\Admin\Setup\BlcName;
use App\Model\Admin\Setup\RoomCategory;
use App\Model\Admin\Setup\RoomFacility;
use App\Model\Admin\Setup\RoomType;
use Auth;
use App\Http\Requests\BlcRoomTypeRequest;
use DB;

class BlcRoomTypeController extends Controller
{
    public function blcRoomTypeView(){
    	$allData = RoomType::where('status','1')->get();
    	return view('backend.setup.room_type.view-room-type',compact('allData'));
    }

    public function blcRoomTypeAdd(){
    	return view('backend.setup.room_type.add-room-type');
    }

    public function blcRoomTypeStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:room_types,name'
        ]);
    	$rmtype = new RoomType();
    	$rmtype->name = $request->name;
    	$rmtype->created_by = Auth::user()->save();
    	$rmtype->save();
    	return redirect()->route('setup.blc.room-type.view')->with('success','Data Saved Successfully');
    }

    public function blcRoomTypeEdit($id){
    	$data['editData'] = RoomType::find($id);
    	return view('backend.setup.room_type.add-room-type',$data);
    }

    public function blcRoomTypeUpdate(BlcRoomTypeRequest $request,$id){
    	$rmtype = RoomType::find($id);
    	$rmtype->name = $request->name;
    	$rmtype->updated_by = Auth::user()->save();
    	$rmtype->save();
    	return redirect()->route('setup.blc.room-type.view')->with('success','Data Updated Successfully');
    }

    public function blcRoomTypeDelete($id){
    	
    }
}
