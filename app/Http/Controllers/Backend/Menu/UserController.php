<?php

namespace App\Http\Controllers\Backend\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\UserRoleType;
use App\Model\Admin\Setup\BlcName;
use Auth;
use App\Http\Requests\UserRequest;
use DB;

class UserController extends Controller
{
	public function getUserRole(Request $request){
		$user_role_type_id = $request->user_role_type_id;
		$allRole = Role::where('user_role_type_id',$user_role_type_id)->get();
		return response()->json($allRole);
	}

    public function userView(){
    	$data['allData'] = User::all();
    	return view('backend.user.view-user',$data);
    }

    public function userAdd(){
    	$data['user_role_types'] = UserRoleType::where('status','1')->get();
    	return view('backend.user.add-user',$data);
    }

    public function userStore(Request $request){
    	// dd('ok');
    	$this->validate($request,[
    		'name'=>'required',
    		'email'=>'required|unique:users,email'
    	]);
    	$data = new User();
    	$data->pin = $request->pin;
    	$data->name = $request->name;
    	$data->designation = $request->designation;
    	$data->mobile_no = $request->mobile_no;
    	$data->email = $request->email;
    	$data->user_role_type_id = $request->user_role_type_id;
    	$data->role_id = $request->role_id;
    	$data->password = bcrypt($request->password);
    	$data->save();
    	return redirect()->route('user.view')->with('success','Data Saved successfully');
    }

    public function userEdit($id){
        $data['editData'] = User::find($id);
        $data['user_role_types'] = UserRoleType::where('status','1')->get();
        return view('backend.user.add-user',$data);
    }

    public function userUpdate(UserRequest $request,$id){
        $data = User::find($id);
    	$data->pin = $request->pin;
    	$data->name = $request->name;
    	$data->designation = $request->designation;
    	$data->mobile_no = $request->mobile_no;
    	$data->email = $request->email;
    	$data->user_role_type_id = $request->user_role_type_id;
    	$data->role_id = $request->role_id;
    	$data->save();
        return redirect()->route('user.view')->with('success','Data updated successfully');
    }

    public function delete($id){
        $user = User::find($id);
        if (file_exists('public/upload/user_images/' . $user->image) AND ! empty($user->image)) {
            unlink('public/upload/user_images/' . $user->image);
        }
        $user->delete();
        return redirect()->route('users.view')->with('success','Data Deleted successfully');
    }

    //BLC Base User Management
    public function blcUserView(){
    	$data['allData'] = User::where('blc_name_id','!=','')->get();
    	return view('backend.user.view-blc-based-user',$data);
    }

    public function blcUserAdd(){
    	$data['user_role_types'] = UserRoleType::where('status','1')->get();
    	$data['blc_names'] = BlcName::where('status','1')->get();
    	return view('backend.user.add-blc-based-user',$data);
    }

    public function blcUserStore(Request $request){
    	// dd('ok');
    	$this->validate($request,[
    		'name'=>'required',
    		'email'=>'required|unique:users,email'
    	]);
    	$data = new User();
    	$data->pin = $request->pin;
    	$data->name = $request->name;
    	$data->designation = $request->designation;
    	$data->mobile_no = $request->mobile_no;
    	$data->email = $request->email;
    	$data->user_role_type_id = $request->user_role_type_id;
    	$data->role_id = $request->role_id;
    	$data->blc_name_id = $request->blc_name_id;
    	$data->password = bcrypt($request->password);
    	$data->save();
    	return redirect()->route('user.blc.view')->with('success','Data Saved successfully');
    }

    public function blcUserEdit($id){
        $data['editData'] = User::find($id);
        $data['user_role_types'] = UserRoleType::where('status','1')->get();
        $data['blc_names'] = BlcName::where('status','1')->get();
        return view('backend.user.add-blc-based-user',$data);
    }

    public function blcUserUpdate(UserRequest $request,$id){
        $data = User::find($id);
    	$data->pin = $request->pin;
    	$data->name = $request->name;
    	$data->designation = $request->designation;
    	$data->mobile_no = $request->mobile_no;
    	$data->email = $request->email;
    	$data->user_role_type_id = $request->user_role_type_id;
    	$data->role_id = $request->role_id;
    	$data->blc_name_id = $request->blc_name_id;
    	$data->save();
        return redirect()->route('user.blc.view')->with('success','Data updated successfully');
    }
}
