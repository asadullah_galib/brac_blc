<?php

namespace App\Http\Controllers\Backend\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Model\UserRoleType;
use Auth;
use App\Http\Requests\RoleRequest;
use DB;

class RoleController extends Controller
{
    public function roleView(){
        $allData = Role::where('status','1')->get();
        return view('backend.role.view-user-role',compact('allData'));
    }

    public function roleAdd(){
        $data['roletypes'] = UserRoleType::where('status','1')->get();
        return view('backend.role.add-user-role',$data);
    }

    public function roleStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:roles,name'
        ]);
        $role = new Role();
        $role->user_role_type_id = $request->user_role_type_id;
        $role->name = $request->name;
        $role->created_by = Auth::user()->save();
        $role->save();
        return redirect()->route('user.role.view')->with('success','Data Saved Successfully');
    }

    public function roleEdit($id){
        $data['roletypes'] = UserRoleType::where('status','1')->get();
        $data['editData'] = Role::find($id);
        return view('backend.role.add-user-role',$data);
    }

    public function roleUpdate(RoleRequest $request,$id){
        $role = Role::find($id);
        $role->user_role_type_id = $request->user_role_type_id;
        $role->name = $request->name;
        $role->updated_by = Auth::user()->save();
        $role->save();
        return redirect()->route('user.role.view')->with('success','Data Updated Successfully');
    }

    public function roleDelete($id){
        
    }
}
