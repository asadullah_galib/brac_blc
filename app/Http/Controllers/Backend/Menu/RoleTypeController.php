<?php

namespace App\Http\Controllers\Backend\Menu;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\UserRoleType;
use Auth;
use App\Http\Requests\UserRoleTypeRequest;
use DB;

class RoleTypeController extends Controller
{
    public function roleTypeView(){
    	$allData = UserRoleType::where('status','1')->get();
    	return view('backend.role_type.view-role-type',compact('allData'));
    }

    public function roleTypeAdd(){
    	return view('backend.role_type.add-role-type');
    }

    public function roleTypeStore(Request $request){
        $this->validate($request,[
            'name'=>'required|unique:user_role_types,name'
        ]);
    	$rmtype = new UserRoleType();
    	$rmtype->name = $request->name;
    	$rmtype->created_by = Auth::user()->save();
    	$rmtype->save();
    	return redirect()->route('user.role.type.view')->with('success','Data Saved Successfully');
    }

    public function roleTypeEdit($id){
    	$data['editData'] = UserRoleType::find($id);
    	return view('backend.role_type.add-role-type',$data);
    }

    public function roleTypeUpdate(UserRoleTypeRequest $request,$id){
    	$rmtype = UserRoleType::find($id);
    	$rmtype->name = $request->name;
    	$rmtype->updated_by = Auth::user()->save();
    	$rmtype->save();
    	return redirect()->route('user.role.type.view')->with('success','Data Updated Successfully');
    }

    public function roleTypeDelete($id){
    	
    }
}
