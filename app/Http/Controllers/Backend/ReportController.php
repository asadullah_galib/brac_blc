<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ResourcePersonProject;
use App\Model\ResourceContribution;
use App\Model\Project;
use App\Model\ResourcePerson;
use DateTime;
use DB;

class ReportController extends Controller
{
    public function manMonth()
    {
		  // $projects = [];
		  // $manmonth = [];
		  // $project = Project::with(['resource_project'])->get();
		  // dd($project->toArray());

            // dd($interval->m);
		  // foreach($project as $key => $p){
		  // 	$fdate = $p->start_date;
    //         $tdate = $p->end_date;
    //         $datetime1 = new DateTime($fdate);
    //         $datetime2 = new DateTime($tdate);
    //         $interval = $datetime1->diff($datetime2);
		  //   // $projects[$key]['project_id'] = $p->id;
		  //   // $projects[$key]['project_name'] = $p->name;
		  //   $projects[$key]['count_resource'] = count($p->resource_project);
		  //   $manmonth[$key] = $projects[$key]['count_resource']*$interval->m;
		  // }		    
    	$data['project_info'] = Project::with(['clients'])->get();
    	$consume_month = [];
    	foreach($data['project_info'] as $key => $p)
    	{
    		$consume_month[$p->id] = ResourceContribution::where('project_id',$p['id'])->sum('working_hour');
    		$consume_month[$p->id] = $consume_month[$p->id]/9.5;
    		$consume_month[$p->id] = round($consume_month[$p->id]/30,2);
    	}

    	
        return view('backend.report.man_month.man-month-report',compact('consume_month'))->with($data);
    }

    public function individualManMonth($id)
    {


        $resource_contributions_person = ResourceContribution::select(DB::raw('resource_person_id'))->with(['resource_persons'])->where('project_id',$id)->groupBy('resource_person_id')->get();
        $data = [];
        // dd()
        foreach ($resource_contributions_person as $key => $value) {
            $data[$key]['project_id']= Project::find($id)['id'];
            // $html[$key]['project_name']= Project::find($id)['name'];
            $data[$key]['resource_id']= $value['resource_persons']['id'];
            $data[$key]['name']=$value['resource_persons']['name']; 
            $html[$key]['resourse_person_working_hour']= ResourceContribution::where([['project_id',$id],['resource_person_id',$value['resource_persons']['id']]])->sum('working_hour');
            $hr = $html[$key]['resourse_person_working_hour']/9.5;
            $data[$key]['hr'] = round($hr,2);
            $month = $hr/30;
            $data[$key]['month'] = round($month,2);
            
        }
        // dd($data);


        return view('backend.report.man_month.individual-man-month',compact('data'));

        // $data['person'] = ResourcePerson::whereHas('resource_contribution',function($q){
        //     $q->where('')
        // })->with(['resource_contribution'])->get();

        // dd($data['contribution']->toArray());
        // return view('backend.report.individual-man-month');

    }
    public function individualManMonthDetails()
    {
        $data['contribution'] = ResourceContribution::with('projects','projects.clients','resource_persons')->where('project_id',request()->project_id)->where('resource_person_id',request()->resource_id)->get();
        // dd($data['contribution']->toArray());

        $data['information'] = ResourceContribution::where('project_id',request()->project_id)->where('resource_person_id',request()->resource_id)->first();
        // dd($data['information']->toArray());
        return view('backend.report.man_month.individual-month-details')->with($data);
    }
}
