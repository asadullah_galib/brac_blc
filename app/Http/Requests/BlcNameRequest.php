<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlcNameRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(isset($this->id)){
            return [
                'name'  => 'required|unique:blc_names,name,'.$this->id
            ];
        }
        return [
            'name'=>'required|unique:blc_names',
        ];
    }
}
