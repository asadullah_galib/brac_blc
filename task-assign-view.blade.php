@php
  $c = 1;
@endphp
@extends('backend.layouts.app')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Manage Task Assign</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Task Assign</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        	<div class="col-lg-12">
	        	<div class="card"> 
	        		<div class="card-header">
	        			<a href="{{route('task.assign.add')}}" class="btn btn-sm btn-info"><i class="fas fa-plus"></i> Add Task Assign</a>
	        		</div>
		            <div class="card-body">
		              <table id="dataTable" class="table table-sm table-bordered table-striped">
		                <thead>
		                <tr>
		                  <th>#</th>
		                  <th>Task Contrubution</th>
                      <th>Resource Person</th>
		                  <th>From Date</th>
                      <th>To Date</th>
                      <th>File</th>
                      
                      <th width="80">Action</th>
		                </tr>
		                </thead>
		                <tbody>
		                @foreach($task_assign as $p)
                    @foreach($p['contributions'] as $r)	
		                <tr>		                  	                 
		                  <td> {{$c}}</td>
                      <td>{{ $r['contribution'] }}</td>
                       <td>{{ $p['resource_persons']['name'] }}</td>
                       <td>{{ $p['from_date'] }}</td>
                       <td>{{ $p['to_date'] }}</td>
                       <td><a href="{{ asset('public/uploads/task_assign/'.$p['file']) }}">{{ !empty($p['file'])? 'Download':'' }}</a></td>
                        
                        
		                  	<td><a href="{{ route('task.assign.edit',$p->id) }}" class="btn btn-primary btn-flat btn-sm edit" data-type="image" data-id="" data-table="Slider"><i class="fas fa-edit"></i></a> | 
                          <a  class="delete btn btn-danger btn-flat btn-sm " data-route="{{ route('project-management.task.assign.delete') }}" data-id="{{ $p['id'] }}" ><i class="fas fa-trash"></i></a>
		                  </td>
                      {{ $c++  }}
		                </tr>
                    @endforeach
		                @endforeach                
		                </tbody>                
		              </table>
		            </div>
	            <!-- /.card-body -->
          		</div>
          <!-- /.card -->
        	</div>
        </div>
      </div>
      <!--/. container-fluid -->
    </section>
@endsection

