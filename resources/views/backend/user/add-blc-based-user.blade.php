@extends('backend.layouts.app')
@section('content')

<div class="col-xl-12">
	<div class="breadcrumb-holder">
		<h3 class="main-title float-left">Manage BLC Base User</h3>
		<ol class="breadcrumb float-right">
			<li class="breadcrumb-item"><a href="{{route('dashboard')}}"><strong>Home</strong></a></li>
			<li class="breadcrumb-item active">User</li>
		</ol>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container fullbody">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>
					@if(@$editData)
					Edit BLC Base User
					@else
					Add BLC Base User
					@endif
                  	<a class="btn btn-success float-right btn-sm" href="{{route('user.blc.view')}}"><i class="fa fa-list"></i> BLC Base User List</a>
                </h4>
			</div>
			<div class="card-body">
				<form method="post" action="{{(@$editData)?route('user.blc.update',$editData->id):route('user.blc.store')}}" id="myForm" enctype="multipart/form-data">
					@csrf
					<div class="form-row">
						<div class="form-group col-md-2">
							<input type="tex" name="pin" value="{{@$editData->pin}}" class="form-control" placeholder="PIN">
						</div>
						<div class="form-group col-md-2">
							<a class="btn btn-success">Search</a>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-4">
							<label>User Name</label>
							<input type="text" name="name" value="{{@$editData->name}}" class="form-control" placeholder="Write User Name">
						</div>
						<div class="form-group col-md-4">
							<label>Designation</label>
							<input type="text" name="designation" value="{{@$editData->designation}}" class="form-control" placeholder="Write Designation">
						</div>
						<div class="form-group col-md-4">
							<label>Mobile No</label>
							<input type="text" name="mobile_no" value="{{@$editData->mobile_no}}" class="form-control" placeholder="Write Mobile No">
						</div>
						<div class="form-group col-md-4">
							<label>Email</label>
							<input type="email" name="email" value="{{@$editData->email}}" class="form-control" placeholder="Write Email Address">
							<font style="color: red">{{($errors->has('email'))?($errors->first('email')):''}}</font>
						</div>
						@if(!@$editData)
						<div class="form-group col-md-4">
							<label for="password">Password</label>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						<div class="form-group col-md-4">
							<label for="password">Confirm Password</label>
							<input type="password" name="password2" class="form-control">
						</div>
						@endif
						<div class="form-group col-md-4">
							<label>Role Type</label>
							<select name="user_role_type_id" id="user_role_type_id" class="form-control">
								<option value="">Select Type</option>
		                        @foreach($user_role_types as $rtype)
		                        <option value="{{$rtype->id}}" {{(@$editData->user_role_type_id==$rtype->id)?"selected":""}}>{{$rtype->name}}</option>
		                        @endforeach
							</select>
						</div>
						<div class="form-group col-md-4">
							<label>Role Name</label>
							<select name="role_id" id="role_id" class="form-control">
								<option value="">Select User Role</option>
							</select>
						</div>
						<div class="form-group col-md-4">
							<label>BLC Name</label>
							<select name="blc_name_id" id="blc_name_id" class="form-control">
								<option value="">Select BLC Name</option>
		                        @foreach($blc_names as $bname)
		                        <option value="{{$bname->id}}" {{(@$editData->blc_name_id==$bname->id)?"selected":""}}>{{$bname->name}}</option>
		                        @endforeach
							</select>
						</div>
						<div class="form-group col-md-12">
							<button type="submit" class="btn btn-primary">{{(@$editData)?"Update":"Submit"}}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>

<script type="text/javascript">
	$(function(){
		$(document).on('change','#user_role_type_id',function(){
			var user_role_type_id = $(this).val();
			$.ajax({
				url:"{{route('get-user-role')}}",
				type:"GET",
				data:{user_role_type_id:user_role_type_id},
				success:function(data){
					var html = '<option value="">Select Role</option>';
					$.each(data,function(key,v){
						html +='<option value="'+v.id+'">'+v.name+'</option>';
					});
					$('#role_id').html(html);
					var role_id = "{{@$editData->role_id}}";
					if(role_id !=''){
						$('#role_id').val(role_id);
					}
				}
			});
		});
	});
</script>

<script type="text/javascript">
	$(function(){
		var user_role_type_id = "{{@$editData->user_role_type_id}}";
		if(user_role_type_id){
			$('#user_role_type_id').val(user_role_type_id).trigger('change');
		}
	});
</script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#myForm').validate({
			rules: {
				user_role_type_id: {
					required: true,
				},
				role_id: {
					required: true,
				},
				blc_name_id: {
					required: true,
				},
				pin: {
					required: true,
				},
				name: {
					required: true,
				},
				designation: {
					required: true,
				},
				mobile_no: {
					required: true,
				},
				email: {
					required: true,
					email: true,
				},
				password : {
					required : true,
					minlength : 6
				},
				password2 : {
					required : true,
					equalTo : '#password'
				}
			},
			messages: {
				email : {
					required : 'Please enter email address',
					email : 'Please enter a <em>valid</em> email address',
				},
				password : {
					required : 'Please enter password',
					minlength : 'Password will be minimum 6 characters or numbers',
				},
				password2 : {
					required : 'Please enter confirm password',
					equalTo : 'Confirm password does not match',
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>
@endsection