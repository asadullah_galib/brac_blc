@extends('backend.layouts.app')
@section('content')

<div class="col-xl-12">
  <div class="breadcrumb-holder">
    <h3 class="main-title float-left">Manage User</h3>
    <ol class="breadcrumb float-right">
      <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><strong>Home</strong></a></li>
      <li class="breadcrumb-item active">User</li>
    </ol>
    <div class="clearfix"></div>
  </div>
</div>
<div class="container fullbody">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h4>User List
          <a class="btn btn-success float-right btn-sm" href="{{route('user.add')}}"><i class="fa fa-plus-circle"></i> Add User</a>
        </h4>
      </div>
      <div class="card-body">
        <table id="example1" class="table table-sm table-bordered">
          <thead>
            <tr>
              <th width="8%">SL.</th>
              <th>Role Type</th>
              <th>Role</th>
              <th>Name</th>
              <th>Email</th>
              <th>Designation</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($allData as $key => $val)
            <tr>
              <td>{{$key+1}}</td>
              <td>{{$val['user_role_type']['name']}}</td>
              <td>{{$val['role']['name']}}</td>
              <td>{{$val->name}}</td>
              <td>{{$val->email}}</td>
              <td>{{$val->designation}}</td>
              <td>
                <a title="Edit" class="btn btn-sm btn-primary" href="{{route('user.edit',$val->id)}}"><i class="fa fa-edit"></i></a>

                <!-- <a title="Delete" id="delete" class="btn btn-sm btn-danger" href=""><i class="fa fa-trash"></i></a> -->
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>  
</div>]

@endsection