@extends('backend.layouts.app')
@section('content')

<div class="col-xl-12">
	<div class="breadcrumb-holder">
		<h3 class="main-title float-left">Manage BLC Name</h3>
		<ol class="breadcrumb float-right">
			<li class="breadcrumb-item"><a href="{{route('dashboard')}}"><strong>Home</strong></a></li>
			<li class="breadcrumb-item active">Name</li>
		</ol>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container fullbody">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>
					@if(@$editData)
					Edit BLC Name
					@else
					Add BLC Name
					@endif
                  	<a class="btn btn-success float-right btn-sm" href="{{route('setup.blc.name.view')}}"><i class="fa fa-list"></i> BLC Name List</a>
                </h4>
			</div>
			<div class="card-body">
				<form method="post" action="{{(@$editData)?route('setup.blc.name.update',$editData->id):route('setup.blc.name.store')}}" id="myForm" enctype="multipart/form-data">
					@csrf
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>BLC Category</label>
							<select name="blc_category_id" class="form-control">
								<option value="">Select Category Name</option>
		                        @foreach($blcCategories as $cat)
		                        <option value="{{$cat->id}}" {{(@$editData->blc_category_id==$cat->id)?"selected":""}}>{{$cat->name}}</option>
		                        @endforeach
							</select>
						</div>
						<div class="form-group col-md-6">
							<label>BLC Name</label>
							<input type="text" name="name" value="{{@$editData->name}}" class="form-control" placeholder="Write BLC Name">
							<font color="red">{{($errors->has('name'))?($errors->first('name')):''}}</font>
						</div>
						<div class="form-group col-md-8">
							<button type="submit" class="btn btn-primary">{{(@$editData)?"Update":"Submit"}}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('#myForm').validate({
			rules: {
				blc_category_id: {
					required: true,
				},
				name: {
					required: true,
				}
			},
			messages: {

			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>

@endsection