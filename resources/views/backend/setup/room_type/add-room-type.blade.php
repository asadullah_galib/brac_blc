@extends('backend.layouts.app')
@section('content')

<div class="col-xl-12">
	<div class="breadcrumb-holder">
		<h3 class="main-title float-left">Manage Room Type</h3>
		<ol class="breadcrumb float-right">
			<li class="breadcrumb-item"><a href="{{route('dashboard')}}"><strong>Home</strong></a></li>
			<li class="breadcrumb-item active">Type</li>
		</ol>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container fullbody">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>
					@if(@$editData)
					Edit Room Type
					@else
					Add Room Type
					@endif
                  	<a class="btn btn-success float-right btn-sm" href="{{route('setup.blc.room-type.view')}}"><i class="fa fa-list"></i> Room Type List</a>
                </h4>
			</div>
			<div class="card-body">
				<form method="post" action="{{(@$editData)?route('setup.blc.room-type.update',$editData->id):route('setup.blc.room-type.store')}}" id="myForm" enctype="multipart/form-data">
					@csrf
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Room Type Name</label>
							<input type="text" name="name" value="{{@$editData->name}}" class="form-control" placeholder="Write Room Type Name">
							<font color="red">{{($errors->has('name'))?($errors->first('name')):''}}</font>
						</div>
						<div class="form-group col-md-8">
							<button type="submit" class="btn btn-primary">{{(@$editData)?"Update":"Submit"}}</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$('#myForm').validate({
			rules: {
				name: {
					required: true,
				}
			},
			messages: {

			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>

@endsection