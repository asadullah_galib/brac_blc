@extends('backend.layouts.app')
@section('content')

<div class="col-xl-12">
	<div class="breadcrumb-holder">
		<h3 class="main-title float-left">Manage Room Facilities</h3>
		<ol class="breadcrumb float-right">
			<li class="breadcrumb-item"><a href="{{route('dashboard')}}"><strong>Home</strong></a></li>
			<li class="breadcrumb-item active">Facilities</li>
		</ol>
		<div class="clearfix"></div>
	</div>
</div>
<div class="container fullbody">
	<div class="col-md-12">
		<div class="card">
			<div class="card-header">
				<h4>Room Facilities List
                  <a class="btn btn-success float-right btn-sm" href="{{route('setup.blc.room-facilities.add')}}"><i class="fa fa-plus-circle"></i> Add Room Facilities</a>
                </h4>
			</div>
			<div class="card-body">
				<table id="example1" class="table table-sm table-bordered">
					<thead>
						<tr>
							<th width="8%">SL.</th>
							<th>Room Facility Title</th>
							<th>Facility Icon</th>
							<th width="10%">Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($allData as $key => $val)
	                    <tr>
	                      <td>{{$key+1}}</td>
	                      <td>{{$val->name}}</td>
	                      <td><img src="{{(!empty($val->image))?url('public/upload/room_facility_icon/'.$val->image):url('public/upload/no_image.png')}}" style="height: 80px;width: 80px;"></td>
	                      <td>
	                        <a title="Edit" class="btn btn-sm btn-primary" href="{{route('setup.blc.room-facilities.edit',$val->id)}}"><i class="fa fa-edit"></i></a>

	                        <!-- <a title="Delete" id="delete" class="btn btn-sm btn-danger" href=""><i class="fa fa-trash"></i></a> -->
	                      </td>
	                    </tr>
	                    @endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</div>

@endsection