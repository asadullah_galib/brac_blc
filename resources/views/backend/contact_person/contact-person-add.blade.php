@extends('backend.layouts.app') @section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Manage Contact Person</h1>
            </div>
            <!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Contact Person</li>
                </ol>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{route('setup-management.contact.view')}}" class="btn btn-info btn-sm"><i class="fas fa-stream"></i> View Contact Person</a>
                    </div>
                    <div class="card-body">
                        <form action="{{!empty($editData)? route('setup-management.contact.update',$editData->id) : route('setup-management.contact.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-sm-6">
                                    <label>Contact Person Name*</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" autocomplete="off" value="{{ !empty($editData->name)? $editData->name : old('name') }}"> @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Contact Person Project*</label>
                                    <select class="form-control @error('project_id') is-invalid @enderror" name="project_id" required>
                                        <option value="">--Select Project--</option>
                                        @foreach($project as $p)
                                        <option {{ @$editData->project_id == $p->id ? 'selected':''}} value="{{$p->id}}">{{$p->name }}{{ " ".'( '.$p['clients']['name'].' )' }} </option>
                                        @endforeach
                                    </select>
                                    @error('project_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>

                                <div class="form-group col-sm-6">
                                    <label>Contact Person Email*</label>
                                    <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" autocomplete="off" value="{{ !empty($editData->email)? $editData->email : old('email') }}"> @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Contact Person Mobile*</label>
                                    <input type="text" class="form-control @error('mobile_no') is-invalid @enderror" name="mobile_no" autocomplete="off" value="{{ !empty($editData->mobile_no)? $editData->mobile_no : old('mobile_no') }}"> @error('mobile_no')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>
                                <div class="form-group col-sm-6">
                                    <label>Contact Person Designation*</label>
                                    <input type="text" class="form-control @error('designation') is-invalid @enderror" name="designation" autocomplete="off" value="{{ !empty($editData->designation)? $editData->designation : old('designation') }}"> @error('designation')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>

                                <div class="form-group col-sm-6">
                                    <label>Contact Person Address*</label>
                                    <textarea type="text" class="form-control textarea @error('address') is-invalid @enderror" name="address" rows="5">{{ !empty($editData->address)? $editData->address : old('address') }}</textarea>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span> @enderror
                                </div>
                            </div>

                            <button class="btn bg-gradient-success btn-flat"><i class="fas fa-save"></i> {{ !empty($editData)? 'Update' : 'Save' }}</button>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
    <!--/. container-fluid -->
</section>
@endsection