<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Brac Learning Center</title>

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/backend/css/adminlte.min.css')}}">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/summernote/summernote-bs4.css')}}">
  <!-- select2 -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/select2/css/select2.min.css')}}">
   <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/sweetalert2/sweetalert2.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/toastr/toastr.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('public/backend/plugins/datatables/dataTables.bootstrap4.css')}}">
  <!-- Handle bar -->
  <script src="{{asset('public/backend/js/handlebars-v4.0.12.js')}}"></script>
  <!-- Css for tree view -->
  <link rel="stylesheet" type="text/css" href="{{asset('public/backend/plugins/jstree/style.css')}}">
   <!-- daterange picker -->
  <link rel="stylesheet" href="{{ asset('public/backend') }}/plugins/daterangepicker/daterangepicker.css">
   <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('public/backend') }}/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="{{asset('public/backend/css/float.css')}}">
  
  <script src="{{asset('public/backend/plugins/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('public/backend/js/float.js')}}"></script>

  <!-- <link rel="stylesheet" href="{{asset('public/backend/table/plugins/fontawesome-free/css/all.min.css')}}"> -->
  <!-- Ionicons -->
 <!--  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- fullCalendar -->
  <link rel="stylesheet" href="{{asset('public/backend/table/plugins/fullcalendar/main.min.css')}}">
  <!-- <link rel="stylesheet" href="../plugins/fullcalendar-interaction/main.min.css"> -->
  <!-- <link rel="stylesheet" href="{{asset('public/backend/table/plugins/fullcalendar-daygrid/main.min.css')}}"> -->
  <!-- <link rel="stylesheet" href="{{asset('public/backend/table/plugins/fullcalendar-timegrid/main.min.css')}}"> -->
  <!-- <link rel="stylesheet" href="{{asset('public/backend/table/plugins/fullcalendar-bootstrap/main.min.css')}}"> -->
  <!-- Theme style -->
  <<!-- link rel="stylesheet" href="{{asset('public/backend/dist/css/adminlte.min.css')}}"> -->
  <!-- Google Font: Source Sans Pro -->
 <!--  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
  
  <style type="text/css">
    .nav-tabs .nav-item {
        margin-bottom: 0px;
    }
    input[type="file"]{
      padding: 0.175rem 0.75rem;
    }
    /*start select2*/
    .select2-container--default .select2-selection--multiple .select2-selection__choice{
      background-color: #17a2b8 !important;
      border-color: #17a2b8 !important;
    }
    .select2-container--default .select2-selection--multiple .select2-selection__choice__remove{
      color: #000 !important;
    }

    .select2-selection--single, .select2-selection__arrow{
      height: 37px !important;
    }

    .select2-selection--single .select2-selection__rendered{
      line-height: 30px !important;
    }
    .select2-selection--multiple, .select2-selection__arrow{
      min-height: 37px !important;
    }

    .select2-selection--multiple .select2-selection__rendered{
      line-height: 26px !important;
    }

    /*end select2*/



    .bg-gradient-success{
      background: #17a2b8 !important;
      border-color: #17a2b8 !important;
    }
  </style>

  <!-- jQuery -->

</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    @include('backend.layouts.navbar')
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('backend.layouts.sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @yield('content')
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <strong>Copyright &copy; {{date('Y')}} <a target="_blank" href="http://www.nanoit.biz">Nano Information Technology</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- Bootstrap -->
<script src="{{asset('public/backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('public/backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>

<!-- DataTables -->
<script src="{{asset('public/backend/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('public/backend/plugins/datatables/dataTables.bootstrap4.js')}}"></script>

<!-- AdminLTE App -->
<script src="{{asset('public/backend/js/adminlte.js')}}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="{{asset('public/backend/js/demo.js')}}"></script>
<script src="{{ asset('public/backend') }}/plugins/moment/moment.min.js"></script>
<script src="{{ asset('public/backend') }}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('public/backend') }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{asset('public/backend/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- select2 -->
<script src="{{asset('public/backend/plugins/select2/js/select2.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('public/backend/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<!-- Js for tree view -->
<script src="{{asset('public/backend/plugins/jstree/jstree.js')}}"></script>
<!-- Validate js -->
<script src="{{ asset('public/backend/js/validate.min.js') }}"></script>
<script src="{{ asset('public/backend/js/additional-methods.js') }}"></script>
<!-- Toastr -->
<script src="{{asset('public/backend/plugins/toastr/toastr.min.js')}}"></script>
<!-- Notifyjs -->
<script src="{{asset('public/backend/js/notify.js')}}"></script>

<!-- jQuery -->
<!-- <script src="{{asset('public/backend/table/plugins/jquery/jquery.min.js')}}"></script> -->
<!-- Bootstrap -->
<!-- <script src="{{asset('public/backend/table/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script> -->
<!-- jQuery UI -->
<script src="{{asset('public/backend/table/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- AdminLTE App -->
<!-- <script src="{{asset('public/backend/dist/js/adminlte.min.js')}}"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="{{asset('public/backend/dist/js/demo.js')}}"></script> -->
<!-- fullCalendar 2.2.5 -->
<script src="{{asset('public/backend/table/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('public/backend/table/plugins/fullcalendar/main.min.js')}}"></script>
<script src="{{asset('public/backend/table/plugins/fullcalendar-daygrid/main.min.js')}}"></script>
<script src="{{asset('public/backend/table/plugins/fullcalendar-timegrid/main.min.js')}}"></script>
<script src="{{asset('public/backend/table/plugins/fullcalendar-interaction/main.min.js')}}"></script>
<script src="{{asset('public/backend/table/plugins/fullcalendar-bootstrap/main.min.js')}}"></script>

<script>
  $(function () {
    $('.singledatepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: false,
      autoUpdateInput: false,
      // drops: "up",
      autoApply:true,
      locale: {
          format: 'DD-MM-YYYY',
          daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
          firstDay: 0
      },
      minDate: '01-01-1930',
  },
  function(start) {
      this.element.val(start.format('DD-MM-YYYY'));
      this.element.parent().parent().removeClass('has-error');
  },
  function(chosen_date) {
      this.element.val(chosen_date.format('DD-MM-YYYY'));
  });
    $('.select2').select2();
    // Summernote
    $('.textarea').summernote();
      //Toastr notification settings
      toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
      }      
  });

  $('.dataTable').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": false,
      "autoWidth": true,
  });

  $(document).on('click','.delete', function(){
      var btn = this;
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        var url         = $(this).data('route');
        var id          = $(this).data('id');
          $.get(url,{id:id}, function(result){
              Swal.fire(
                'Deleted!',
                'Record has been deleted.',
                'success'
              );
              $(btn).closest('tr').fadeOut(1500);
          });
      }     
      
    })
  });     

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.checkboxesTree').jstree({
            'core' : {
                'themes' : {
                    'responsive': false
                }
            },

            'types' : {
                'default' : {
                    'icon' : 'fa fa-file-text-o'
                },
                'file' : {
                    'icon' : 'fa fa-file-text'
                }
            },

            'plugins' : ['types', 'checkbox']
        });
    });
</script>
<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#image').change(function(e){
      var reader = new FileReader();
      reader.onload = function(e){
        $('#showImage').attr('src',e.target.result);
      }
      reader.readAsDataURL(e.target.files['0']);
    });
  });
</script>
@yield('script')
@include('backend.layouts.notification')
</body>
</html>

