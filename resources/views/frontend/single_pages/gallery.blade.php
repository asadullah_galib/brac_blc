@extends('frontend.layouts.app')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Gallery</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active">Gallery</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
<!-- Gallery secion start -->
<div class="gallery-secion content-area-11" style="padding-top: 20px;background-image: url('public/frontend/img/backgnd/gallery-bcknd.jpg');">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1 style="color: #fff;">Our Gallery</h1>
        </div>
        <ul class="list-inline-listing filters filters-listing-navigation">
            <li class="active btn filtr-button filtr" data-filter="all">All</li>
            <li data-filter="1" class="btn btn-inline filtr-button filtr">Classic</li>
            <li data-filter="2" class="btn btn-inline filtr-button filtr">Deluxe</li>
            <li data-filter="3" class="btn btn-inline filtr-button filtr">Royal</li>
            <li data-filter="4" class="btn btn-inline filtr-button filtr">Luxury</li>
        </ul>
        <div class="row">
            <div class="filtr-container">

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="3, 2, 4">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-3.jpg" alt="img-3" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Luxury Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="3, 4, 1">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-2.jpg" alt="img-2" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Double Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 4, 2">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-1.jpg" alt="img-1" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Single Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="2, 3, 1">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-4.jpg" alt="img-4" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Family Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 2, 3">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-5.jpg" alt="img-5" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Standard</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="4, 2, 1">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-6.jpg" alt="img-6" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Couple Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 4, 2">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-7.jpg" alt="img-7" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Single Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="2, 3, 4">
                    <figure class="portofolio-thumb">
                        <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-15-2.jpg" alt="img-8" class="img-responsive"></a>
                        <figcaption>
                            <div class="figure-content">
                                <h3 class="title">Family Room</h3>
                            </div>
                        </figcaption>
                    </figure>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Gallery secion end -->

@endsection