<!DOCTYPE html>
<html lang="en">
<head>
    <title>Brac Learning Centre</title>
    <meta charset="utf-8">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/bootstrap-datepicker.min.css">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('public/frontend')}}/css/skins/blue-light-2.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/ie10-viewport-bug-workaround.css">
    <script  src="{{asset('public/frontend')}}/js/ie-emulation-modes-warning.js"></script>
</head>
<body>

<!-- Content bg area start -->
<div class="contact-bg overview-bgi">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- logo -->
                    <a href="{{url('')}}" class="clearfix alpha-logo">
                        <img src="{{asset('public/frontend')}}/img/logos/logo.png" alt="logo">
                    </a>
                    <!-- details -->
                    <div class="details">
                        <h3>Create an account</h3>
                        <!-- Form start-->
                        <form action="">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="input-text form-control" name="user_type" id="user_type">
                                            <option>Select User Name</option>
                                            <option value="External">External</option>
                                            <option value="BRAC">BRAC</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="first_name" class="input-text" placeholder="First Name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="last_name" class="input-text" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="birth_day" class="input-text datepicker" placeholder="Date of Birth">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="input-text form-control" name="gender">
                                            <option>Select Gender</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="text" name="nid_passport" class="input-text" placeholder="NID/Passport No">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="location" class="input-text" placeholder="Location">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="mobile" class="input-text" placeholder="Mobile No">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="email" name="email" class="input-text" placeholder="Email ID">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="password" name="password" class="input-text" placeholder="Password">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input type="password" name="confirm_Password" class="input-text" placeholder="Confirm Password">
                                    </div>
                                </div>
                                <div class="form-row show_user_type" style="display: none; padding-top: 15px;">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="pin_no" class="input-text" placeholder="Pin">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="designation" class="input-text" placeholder="Designation">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="program" class="input-text" placeholder="Program Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" name="working_place" class="input-text" placeholder="Work Place">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4">
                                    <div class="mb-0">
                                        <button type="submit" class="btn-md btn-theme btn-block" style="max-width: 200px">Signup</button>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </form><br/>
                        <!-- Form end-->
                        <div class="footer">
                            <span>
                                Already a member? <a href="{{route('frontend.login')}}">Login here</a>
                            </span>
                        </div>
                    </div>
                    <!-- Footer -->
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content bg area end -->

<script  src="{{asset('public/frontend')}}/js/jquery-2.2.0.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/bootstrap.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/bootstrap-submenu.js"></script>
<script  src="{{asset('public/frontend')}}/js/jquery.mb.YTPlayer.js"></script>
<script  src="{{asset('public/frontend')}}/js/wow.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/bootstrap-select.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/jquery.easing.1.3.js"></script>
<script  src="{{asset('public/frontend')}}/js/jquery.scrollUp.js"></script>
<script  src="{{asset('public/frontend')}}/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/jquery.filterizr.js"></script>
<script  src="{{asset('public/frontend')}}/js/bootstrap-datepicker.min.js"></script>
<script  src="{{asset('public/frontend')}}/js/app.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="{{asset('public/frontend')}}/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->

<script type="text/javascript">
    $(function(){
        $(document).ready(function(){
        //User Type
            $(document).on('change','#user_type',function(){
                var user_type = $(this).val();
                if(user_type == 'BRAC'){
                    $('.show_user_type').show();
                }else{
                    $('.show_user_type').hide();
                }
            });
        });
    });
</script>

</body>
</html>