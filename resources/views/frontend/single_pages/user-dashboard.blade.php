@extends('frontend.layouts.app')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Dashboard</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active">Dashboard</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
<!-- Gallery secion start -->
<div class="gallery-secion content-area-11" style="padding-top: 20px;background-image: url('public/frontend/img/backgnd/facility-cervice.jpg');">
    <section class="home-content-top">
        <div class="container">
            <div class="tabbable-panel margin-tops4 ">
                <div class="tabbable-line custom_tab">
                    <ul class="nav nav-tabs tabtop  tabsetting">
                        <li class="active">
                            <a href="#tab_default_1" data-toggle="tab"><p style="font-size: 22px;">My Profile</p> </a>
                        </li>
                        <li>
                            <a href="#tab_default_2" data-toggle="tab"> <p style="font-size: 22px;">My Booking</p></a>
                        </li>
                        <li>
                            <a href="#tab_default_3" data-toggle="tab"> <p style="font-size: 22px;">Request</p> </a>
                        </li>
                        <li>
                            <a href="#tab_default_4" data-toggle="tab"> <p style="font-size: 22px;">Complain</p></a>
                        </li>
                    </ul>
                    <div class="tab-content margin-tops">
                        <div class="tab-pane active fade in" id="tab_default_1">
                            <div class="col-md-12">
                                <h4><u>User/Customer Details</u><a class="btn btn-info btn-sm pull-right" href="{{route('frontend.user.edit.info')}}"><i class="fa fa-edit"></i> Edit</a></h4>
                                <div class="row">
                                    <div class="col-md-2">
                                        <p><strong>First Name :</strong></p>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Md. Sumon</p>
                                    </div>
                                    <div class="col-md-2">
                                        <p><strong>Last Name :</strong></p>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Chouduri</p>
                                    </div>
                                    <div class="col-md-2">
                                        <p><strong>Date of Birth :</strong></p>
                                    </div>
                                    <div class="col-md-2">
                                        <p>03-20-1990</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Gender :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Male</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>NID/Passport No :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>201331037063</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Mobile No :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>+88 01928511049</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Email ID :</strong>
                                    </div>
                                    <div class="col-md-10">
                                        <p>test@gmail.com</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Pin :</strong>
                                    </div>
                                    <div class="col-md-4">
                                        <p>xxxxxxxxxxxxx</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Designation :</strong>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Sr. Executive</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Program :</strong>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Night Stay</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Work Place :</strong>
                                    </div>
                                    <div class="col-md-4">
                                        <p>Uttar-badda,Dhaka</p>
                                    </div>
                                </div>
                                <h4>
                                    <u>+Add Address</u>
                                    <a class="btn btn-warning btn-sm pull-right" href=""><i class="fa fa-trash"></i> Delete</a>
                                    <a class="btn btn-success btn-sm pull-right" href="{{route('frontend.user.edit.details')}}"><i class="fa fa-edit"></i> Edit</a>
                                </h4>
                                <div class="row">
                                    <div class="col-md-2">
                                        <strong>House No :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>#420</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Road No :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Block#C</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>City :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Dhaka</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>State :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>xxxxxxxx</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Post Code :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>1300</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Country :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>Bangladesh</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Phone :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>02-895560</p>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>Mobile No :</strong>
                                    </div>
                                    <div class="col-md-2">
                                        <p>+88 01704344126</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_default_2">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 booking-tab-menu">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item active text-center">
                                                <h5>Current Booking</h5>
                                            </a>
                                            <a href="#" class="list-group-item text-center">
                                                <h5>Booking Histry</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 booking-tab">
                                        <!-- flight section -->
                                        <div class="booking-tab-content active">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <p><strong>Order Reference no :</strong></p>
                                                </div>
                                                <div class="col-md-9">
                                                    <p>XXXXXXXXXXXXXX</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p><strong>Check in Date :</strong></p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>30-05-2019</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p><strong>Total Price :</strong></p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>25,000 Tk</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p><strong>BLC Name :</strong></p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>Gulshan BLC</p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p><strong>Due Price :</strong></p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p>7,000 Tk</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Payment Method :</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>Bkash</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Payment Status :</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>Accepted</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Refund Status :</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>Accepted</p>
                                                </div>
                                                <div class="col-md-4"></div>
                                                <div class="col-md-4">
                                                    <a class="btn btn-success btn-sm" href="">Click to see Details</a>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4><u>Room Booked for you</u></h4>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Room Image</strong></p>
                                                </div>
                                                <div class="col-md-10">
                                                    <img src="{{asset('public/frontend')}}/img/room/img-1.jpg" style="width: 100px;height: 90px;">
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Room Type</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>Single Room</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>No of Room</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>536</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Check in Date</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>12-01-2020</p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p><strong>Check out Date</strong></p>
                                                </div>
                                                <div class="col-md-2">
                                                    <p>11-02-2020</p>
                                                </div>
                                                <div class="col-md-1">
                                                    <p><strong>Purpose</strong></p>
                                                </div>
                                                <div class="col-md-7">
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4><u>Room Information</u></h4>
                                                </div>
                                                <div class="col-md-12">
                                                    <table width="100%" class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Room Type</th>
                                                                <th>No of Room</th>
                                                                <th>Room No</th>
                                                                <th>No of Days</th>
                                                                <th>Unit Price</th>
                                                                <th>Total Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Single</td>
                                                                <td>2</td>
                                                                <td>100,101</td>
                                                                <td>2</td>
                                                                <td>1000 Tk</td>
                                                                <td>2000 Tk</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4><u>Additional Facilities Information</u></h4>
                                                </div>
                                                <div class="col-md-12">
                                                    <table width="100%" class="table">
                                                        <thead>
                                                            <tr>
                                                                <th>Facilities Type</th>
                                                                <th>No of Facilities</th>
                                                                <th>Day/Person No</th>
                                                                <th>Unit Price</th>
                                                                <th>Total Price</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>Dinner</td>
                                                                <td>2</td>
                                                                <td>20p</td>
                                                                <td>300</td>
                                                                <td>6000 Tk</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- train section -->
                                        <div class="booking-tab-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>BLC name Stayed in</td>
                                                                <td>Stayed : from 10-02-2020 to 12-02-2020,No of Room : 10, Room No : 101,102,102, No of Days : 2, Purpose : Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>BLC name Stayed in</td>
                                                                <td>Stayed : from 10-02-2020 to 12-02-2020,No of Room : 10, Room No : 101,102,102, No of Days : 2, Purpose : Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_default_3">
                            <div class="col-md-12">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 customer-req-tab-menu">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item active text-center">
                                                <h5>New Request</h5>
                                            </a>
                                            <a href="#" class="list-group-item text-center">
                                                <h5>Request Histry</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 customer-req-tab">
                                        <!-- flight section -->
                                        <div class="customer-req-tab-content active">
                                            <form class="form-row">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <input type="text" name="room_no" class="form-control" placeholder="Write Room No here">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" name="request_name" class="form-control" placeholder="Write your request here">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="submit" class="btn btn-info">Send</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- train section -->
                                        <div class="customer-req-tab-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>Date : 20-03-2020 & 8.00pm</td>
                                                                <td>Description : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td>Gulshan BLC</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>Date : 20-03-2020 & 8.00pm</td>
                                                                <td>Description : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td>Gulshan BLC</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_default_4">
                            <div class="col-md-12">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 complain-tab-menu">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item active text-center">
                                                <h5>New Complain</h5>
                                            </a>
                                            <a href="#" class="list-group-item text-center">
                                                <h5>Complain Histry</h5>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 complain-tab">
                                        <!-- flight section -->
                                        <div class="complain-tab-content active">
                                            <form class="form-row">
                                                <div class="form-group">
                                                    <div class="col-md-3">
                                                        <input type="text" name="room_no" class="form-control" placeholder="Write Room No here">
                                                    </div>
                                                    <div class="col-md-8">
                                                        <input type="text" name="complain" class="form-control" placeholder="Write your complain here">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <button type="submit" class="btn btn-info">Send</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <!-- train section -->
                                        <div class="complain-tab-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>Date : 20-03-2020 & 8.00pm</td>
                                                                <td>Description : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td>Gulshan BLC</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <table width="100%" class="table table-bordered">
                                                        <tbody>
                                                            <tr>
                                                                <td>Date : 20-03-2020 & 8.00pm</td>
                                                                <td>Description : Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry</td>
                                                                <td>Gulshan BLC</td>
                                                                <td><a class="btn btn-info btn-custom" href="">Details</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<!-- Gallery secion end -->

@endsection