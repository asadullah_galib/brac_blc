@extends('frontend.layouts.app')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Blog</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active">Blog</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
<!-- Blog body start -->
<div class="blog-body content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-xs-12">
                <!-- Blog box start -->
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/room/big-img-5.jpg" alt="blog" class="img-responsive">
                        <div class="profile-user">
                            <img src="{{asset('public/frontend')}}/img/avatar/avatar-1.jpg" alt="user">
                        </div>
                    </div>
                    <div class="detail">
                        <div class="post-meta clearfix">
                            <ul>
                                <li>
                                    <strong><a href="#">Antony</a></strong>
                                </li>
                                <li class="mr-0"><span>Feb 31, 2018</span></li>
                                <li class="fr mr-0"><a href="#"><i class="fa fa-commenting-o"></i></a>15</li>
                                <li class="fr"><a href="#"><i class="fa fa-calendar"></i></a>5k</li>
                            </ul>
                        </div>
                        <h3>
                            <a href="">Alpha Hotel pars studiorum</a>
                        </h3>
                        <p>lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. But also the leap into electronic typesetting,</p>
                        <a href="" class="read-more-btn">Read more...</a>
                    </div>
                </div>
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/room/big-img-4.jpg" alt="blog" class="img-responsive">
                        <div class="profile-user">
                            <img src="{{asset('public/frontend')}}/img/avatar/avatar-2.jpg" alt="user">
                        </div>
                    </div>
                    <div class="detail">
                        <div class="post-meta clearfix">
                            <ul>
                                <li>
                                    <strong><a href="#">Teseira</a></strong>
                                </li>
                                <li class="mr-0"><span>Feb 31, 2018</span></li>
                                <li class="fr mr-0"><a href="#"><i class="fa fa-commenting-o"></i></a>15</li>
                                <li class="fr"><a href="#"><i class="fa fa-calendar"></i></a>5k</li>
                            </ul>
                        </div>
                        <h3>
                            <a href="">Best night photo at Alpha hotel in</a>
                        </h3>
                        <p>lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. But also the leap into electronic typesetting,</p>
                        <a href="" class="read-more-btn">Read more...</a>
                    </div>
                </div>
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/room/big-img-3.jpg" alt="blog" class="img-responsive">
                        <div class="profile-user">
                            <img src="{{asset('public/frontend')}}/img/avatar/avatar-3.jpg" alt="user">
                        </div>
                    </div>
                    <div class="detail">
                        <div class="post-meta clearfix">
                            <ul>
                                <li>
                                    <strong><a href="#">John Doe</a></strong>
                                </li>
                                <li class="mr-0"><span>Feb 31, 2018</span></li>
                                <li class="fr mr-0"><a href="#"><i class="fa fa-commenting-o"></i></a>15</li>
                                <li class="fr"><a href="#"><i class="fa fa-calendar"></i></a>5k</li>
                            </ul>
                        </div>
                        <h3>
                            <a href="">Standard Post Format Title</a>
                        </h3>
                        <p>lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. But also the leap into electronic typesetting,</p>
                        <a href="" class="read-more-btn">Read more...</a>
                    </div>
                </div>
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/room/big-img-6.jpg" alt="blog" class="img-responsive">
                        <div class="profile-user">
                            <img src="{{asset('public/frontend')}}/img/avatar/avatar-1.jpg" alt="user">
                        </div>
                    </div>
                    <div class="detail">
                        <div class="post-meta clearfix">
                            <ul>
                                <li>
                                    <strong><a href="#">Antony</a></strong>
                                </li>
                                <li class="mr-0"><span>Feb 31, 2018</span></li>
                                <li class="fr mr-0"><a href="#"><i class="fa fa-commenting-o"></i></a>15</li>
                                <li class="fr"><a href="#"><i class="fa fa-calendar"></i></a>5k</li>
                            </ul>
                        </div>
                        <h3>
                            <a href="">Alpha Hotel pars studiorum</a>
                        </h3>
                        <p>lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.But also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. But also the leap into electronic typesetting,</p>
                        <a href="" class="read-more-btn">Read more...</a>
                    </div>
                </div>
                <!-- Blog box end -->

                <div class="text-center">
                    <!-- Page navigation start -->
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                            <li><a href="">1 <span class="sr-only">(current)</span></a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li class="active"><a href="">4</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- Page navigation end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Blog body end -->

@endsection