@extends('frontend.layouts.app')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>About Us</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active">About Us</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
<!-- About Institute start -->
<div class="about-institute content-area-6" style="background-image: url('public/frontend/img/backgnd/abouts6.jpg');">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-text">
                    <!-- title -->
                    <div class="main-title-2">
                        <h1>ABOUT BLC</h1>
                    </div>
                    <!-- paragraph -->
                    <p style="text-align: justify;color: #000;font-size: 15px">Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.
                    Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="hotels-detail-slider simple-slider">
                    <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                        <div class="carousel-outer">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item">
                                    <img src="{{asset('public/frontend')}}/img/about/about-2.jpg" class="img-preview img-responsive" alt="about-2">
                                </div>
                                <div class="item active left">
                                    <img src="{{asset('public/frontend')}}/img/about/about-3.jpg" class="img-preview img-responsive" alt="about-3">
                                </div>
                                <div class="item next left">
                                    <img src="{{asset('public/frontend')}}/img/about/about-1.jpg" class="img-preview img-responsive" alt="about-1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Institute end -->

@endsection