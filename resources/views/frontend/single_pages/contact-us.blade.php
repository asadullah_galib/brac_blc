@extends('frontend.layouts.app')
@section('content')
<!-- Sub banner start -->
<div class="sub-banner overview-bgi">
    <div class="container">
        <div class="breadcrumb-area">
            <h1>Contact Us</h1>
            <ul class="breadcrumbs">
                <li><a href="{{url('')}}">Home</a></li>
                <li class="active">Contact Us</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->
<!-- Contact 3 start -->
<div class="contact-3 content-area-6">
    <div class="container">
        <div class="main-title mb-60">
            <h1>Send us a message</h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <!-- Contact details start -->
                <div class="contact-info">
                    <h3>Contact Info</h3>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"></i> Address: BRAC Centre, 75 Mohakhali, Dhaka-1212.</li>
                        <li><i class="fa fa-phone"></i> Phone: 880-2-9881265</li>
                        <li><i class="mr-3 fa fa-envelope"></i> Email:info@brac.net</li>
                    </ul>
                </div>
                <!-- Contact details end -->
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                <!-- Contact form start -->
                <div class="contact-form">
                    <form id="contact_form">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group fullname">
                                    <input type="text" name="full-name" class="input-text" placeholder="Full Name">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group enter-email">
                                    <input type="email" name="email" class="input-text" placeholder="Enter email">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group subject">
                                    <input type="text" name="subject" class="input-text" placeholder="Subject">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group number">
                                    <input type="text" name="phone" class="input-text" placeholder="Phone Number">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                                <div class="form-group message">
                                    <textarea class="input-text" name="message" placeholder="Write message" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="send-btn mb-0">
                                    <button type="button" class="btn-md btn-theme btn-custom">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Contact form end -->
            </div>
        </div>
    </div>
</div>
<!-- Contact 3 end -->

<!-- Google map start -->
<div class="section">
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3651.0852366892273!2d90.40797431445641!3d23.77997889356387!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c7767d3aaaab%3A0xd3720a8e274190ec!2sBRAC%20Centre!5e0!3m2!1sen!2sbd!4v1583842258347!5m2!1sen!2sbd" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>
<!-- Google map end -->

@endsection