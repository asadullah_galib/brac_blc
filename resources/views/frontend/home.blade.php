@extends('frontend.layouts.app')
@section('content')
<!-- slider-start -->
@include('frontend.layouts.slider')
<!-- slider-end -->
<!-- about-start -->
@include('frontend.layouts.about')
<!-- about-end -->
<!-- our-blc-start -->
@include('frontend.layouts.our-blc')
<!-- our-blc-end -->
<!-- our-room-start -->
@include('frontend.layouts.our-room')
<!-- our-room-end -->
<!-- news-events-start -->
@include('frontend.layouts.news-events')
<!-- news-events-end -->
<!-- our-services-start -->
@include('frontend.layouts.our-services')
<!-- our-services-end -->
<!-- foods-start -->
@include('frontend.layouts.foods')
<!-- foods-end -->
<!-- gallery-start -->
@include('frontend.layouts.gallery')
<!-- gallery-end -->
<!-- feedback-start -->
@include('frontend.layouts.feedback')
<!-- feedback-end -->

@endsection