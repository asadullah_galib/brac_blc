<!DOCTYPE html>
<html lang="en">
<head>
    <title>Brac Learning Centre</title>
    <meta charset="utf-8">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/bootstrap-datepicker.min.css">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('public/frontend')}}/css/skins/blue-light-2.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('public/frontend')}}/img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/ie10-viewport-bug-workaround.css">
    <script  src="{{asset('public/frontend')}}/js/ie-emulation-modes-warning.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/custom.css">
</head>
<body>

    <div class="page_loader"></div>
    <!-- Top header start -->
    <header class="top-header top-header-3 hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
                    <div class="list-inline">
                        <a href=""><i class="fa fa-phone"></i>880-2-9881265</a>
                        <a href=""><i class="fa fa-envelope"></i> info@brac.net</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                    <ul class="social-list clearfix pull-right">
                        <li>
                            <a href="{{route('frontend.login')}}" class="sign-in"><i class="fa fa-user"></i> Log In / Register</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 60px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">CHECK ROOM AVAILIBILITY 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="blc_name">
                                        <option value="">Select BLC</option>
                                        <option value="Gulshan_blc">Gulshan</option>
                                        <option value="Rajshahi_blc">Rajshahi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" placeholder="Check In Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" placeholder="Check Out Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="purpose">
                                        <option>Select Purpose</option>
                                        <option>Purpose 1</option>
                                        <option>Purpose 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="room">
                                        <option>Select Room</option>
                                        <option>Single Room</option>
                                        <option>Double Room</option>
                                        <option>Deluxe Room</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="no_of_person" placeholder="No of Participant/Person">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                    <button type="button" class="btn btn-info btn-custom">Search</button>
                </div>
            </div>
        </div>
    </div>

    <!-- navbar-start -->
    @include('frontend.layouts.navbar')
    <!-- navbar-end -->

    <!-- content-start-->
    @yield('content')
    <!-- content-end-->

    <!-- footer-start -->
    @include('frontend.layouts.footer')
    <!-- footer-end -->

    <script  src="{{asset('public/frontend')}}/js/jquery-2.2.0.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-submenu.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.mb.YTPlayer.js"></script>
    <script  src="{{asset('public/frontend')}}/js/wow.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-select.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.easing.1.3.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.scrollUp.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.filterizr.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-datepicker.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/app.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script  src="{{asset('public/frontend')}}/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Custom javascript -->

    <!-- Position fixed Script Start -->
    <script type="text/javascript">
        $(window).on('scroll',function(){
            var scroll = $(window).scrollTop();
            if(scroll>41){
                $('.main-header').addClass('fix-pos');
            }else{
                $('.main-header').removeClass('fix-pos');

            }
            console.log(scroll);

        });
    </script>
    <!-- Position fixed Script End -->
    <script type="text/javascript">
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("div.booking-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.booking-tab>div.booking-tab-content").removeClass("active");
                $("div.booking-tab>div.booking-tab-content").eq(index).addClass("active");
            });

            $("div.customer-req-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.customer-req-tab>div.customer-req-tab-content").removeClass("active");
                $("div.customer-req-tab>div.customer-req-tab-content").eq(index).addClass("active");
            });

            $("div.complain-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.complain-tab>div.complain-tab-content").removeClass("active");
                $("div.complain-tab>div.complain-tab-content").eq(index).addClass("active");
            });
        });
    </script>
</body>
</html>