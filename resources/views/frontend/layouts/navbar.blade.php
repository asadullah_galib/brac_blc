<!-- Main header start -->
<header class="main-header main-header-4">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="index.html" class="logo">
                    <img src="{{asset('public/frontend')}}/img/logos/logo.png" alt="logo">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li class="dropdown active">
                        <a href="{{url('')}}">
                            Home
                        </a>
                    </li>
                    <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            About<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('frontend.aboutus')}}">About Us</a></li>
                            <li><a href="{{route('frontend.service')}}">Service</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                            Pages<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{route('frontend.gallery')}}">Gallery</a>
                            </li>
                            <li><a href="{{route('frontend.booking.system')}}">Booking System</a></li>
                            <li><a href="{{route('frontend.faq')}}">Faq</a></li>
                            <li><a href="{{route('frontend.login')}}">Login</a></li>
                            <li><a href="{{route('frontend.signup')}}">Signup</a></li>
                            <li><a href="{{route('frontend.forgot.password')}}">Forgot Password</a></li>
                        </ul>
                    </li>
                    <li><a href="{{route('frontend.blog')}}">Blog</a></li>
                    <li><a href="{{route('frontend.contactus')}}">Contact</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
                    <li>
                        <a class="btn-navbar btn btn-sm btn-theme-sm-outline btn-round" data-toggle="modal" data-target="#exampleModal">Booking</a>
                    </li>
                    <li>
                        <a id="header-search-btn" class="btn-navbar search-icon"><i class="fa fa-search"></i></a>
                    </li>
                </ul>
            </div>

            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>

        <div class="header-search animated fadeInDown">
            <form class="form-inline">
                <input type="text" class="form-control" id="searchKey" placeholder="Search...">
                <div class="search-btns">
                    <button type="submit" class="btn btn-default">Search</button>
                </div>
            </form>
        </div>
    </div>
</header>
<!-- Main header end -->