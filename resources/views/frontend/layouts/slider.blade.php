<!-- Banner start -->
<div class="banner banner-max-height">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{asset('public/frontend')}}/img/slider/slider1.jpg" alt="slider1">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="banner-content text-center">
                        <h1 data-animation="animated fadeInDown delay-05s"><span>Welcome to</span> BLC</h1>
                        <p data-animation="animated fadeInUp delay-1s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing elit <br/>Lorem ipsum dolor sit amet, conconsectetuer</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{asset('public/frontend')}}/img/slider/slider2.jpg" alt="slider2">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="banner-content text-center">
                        <h1 data-animation="animated fadeInLeft delay-05s"><span>Best </span>Room</h1>
                        <p data-animation="animated fadeInUp delay-05s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing elit <br/> Lorem ipsum dolor sit amet, conconsectetuer</p>
                    </div>
                </div>
            </div>
            <div class="item">
                <img src="{{asset('public/frontend')}}/img/slider/slider3.jpg" alt="slider3">
                <div class="carousel-caption banner-slider-inner banner-top-align">
                    <div class="banner-content text-center">
                        <h1 data-animation="animated fadeInLeft delay-05s"><span>Best Place</span> for relax</h1>
                        <p data-animation="animated fadeInLeft delay-1s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing <br/> elit Lorem ipsum dolor sit amet, conconsectetuer</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!-- Search area box start -->
    <div class="search-area-box search-area-box-4 search-area-box-5">
        <div class="container">
            <div class="search-contents search-contents-3" style="background-image: url('public/frontend/img/backgnd/chk-img2.jpg');">
                <form method="GET">
                    <div class="row search-your-details">
                        <div class="col-md-12">
                            <div class="search-your-rooms">
                                <h4 style="color: #fff">Check Room Availibility</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields form-control-2" name="blc_name">
                                                <option value="">Select BLC</option>
                                                <option value="Gulshan_blc">Gulshan</option>
                                                <option value="Rajshahi_blc">Rajshahi</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="text" class="btn-default datepicker" placeholder="Check In Date">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="text" class="btn-default datepicker" placeholder="Check Out Date">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields form-control-2" name="purpose">
                                                <option>Select Purpose</option>
                                                <option>Purpose 1</option>
                                                <option>Purpose 2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields form-control-2" name="room">
                                                <option>Room</option>
                                                <option>Single Room</option>
                                                <option>Double Room</option>
                                                <option>Deluxe Room</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="text" class="btn-default" name="no_of_person" placeholder="No of Participant/Person">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="margin-top: 20px;">
                                <div class="form-group">
                                    <button class="search-button btn-blocks">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area box end -->
</div>
<!-- Banner end -->