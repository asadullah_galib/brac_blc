<!-- Our BLC Partners block start -->
<div class="partners-block" style="background: #3AC4FA;padding: 1px 0px 43px 0px;background-image: url('public/frontend/img/backgnd/feedbck.jpg');">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1>Our BLC</h1>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="carousel our-partners slide" id="ourPartners">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="{{asset('public/frontend')}}/img/blc/barishal.jpg" alt="audiojungle">
                                    <p class="our-blc-para">BLC Barishal</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="{{asset('public/frontend')}}/img/blc/barguna.jpg" alt="audiojungle">
                                    <p class="our-blc-para">BLC Bagura</p> 
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="{{asset('public/frontend')}}/img/blc/coxbz.jpg" alt="audiojungle">
                                    <p class="our-blc-para">BLC Cox's Bazar</p>
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="#">
                                    <img src="{{asset('public/frontend')}}/img/blc/cttg.jpg" alt="audiojungle">
                                    <p class="our-blc-para">BLC Chattogram</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                    <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Our BLC Partners block end -->