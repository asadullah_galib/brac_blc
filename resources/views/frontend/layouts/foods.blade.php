<!-- Food Blog section start -->
<div class="blog-section content-area" style="background-image: url('public/frontend/img/backgnd/fooods.jpg');">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1 style="color: #fff;">Food Items</h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/food/food-1.jpg" alt="food-1" class="img-responsive">
                        <div class="profile-user">
                            250 TK <br/>
                            ONLY
                        </div>
                    </div>
                    <div class="detail">
                        <h3 style="color: #fff;">Fish and Rice</h3>
                        <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 hidden-sm">
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/food/food-2.jpg" alt="food-2" class="img-responsive">
                        <div class="profile-user">
                            350 TK <br/>
                            ONLY
                        </div>
                    </div>
                    <div class="detail">
                        <h3 style="color: #fff;">Tehari and Biryani</h3>
                        <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog-1">
                    <div class="blog-photo">
                        <img src="{{asset('public/frontend')}}/img/food/food-3.jpg" alt="food-3" class="img-responsive">
                        <div class="profile-user">
                            300 TK <br/>
                            ONLY
                        </div>
                    </div>
                    <div class="detail">
                        <h3 style="color: #fff;">Chili Chicken</h3>
                        <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Food Blog section end -->