<!DOCTYPE html>
<html lang="en">
<head>
    <title>Brac Learning Centre</title>
    <meta charset="utf-8">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="{{asset('public/frontend')}}/css/bootstrap-datepicker.min.css">
    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('public/frontend')}}/css/skins/blue-light-2.css">
    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('public/frontend')}}/img/favicon.ico" type="image/x-icon" >
    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/ie10-viewport-bug-workaround.css">
    <script  src="{{asset('public/frontend')}}/js/ie-emulation-modes-warning.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('public/frontend')}}/css/custom.css">
</head>
<body>

    <div class="page_loader"></div>
    <!-- Top header start -->
    <header class="top-header top-header-3 hidden-xs" id="top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
                    <div class="list-inline">
                        <a href=""><i class="fa fa-phone"></i>880-2-9881265</a>
                        <a href=""><i class="fa fa-envelope"></i> info@brac.net</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12">
                    <ul class="social-list clearfix pull-right">
                        <li>
                            <a href="login.html" class="sign-in"><i class="fa fa-user"></i> Log In / Register</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Top header end -->

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="padding-top: 60px;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">CHECK ROOM AVAILIBILITY 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </h4>

                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="blc_name">
                                        <option value="">Select BLC</option>
                                        <option value="Gulshan_blc">Gulshan</option>
                                        <option value="Rajshahi_blc">Rajshahi</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" placeholder="Check In Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control datepicker" placeholder="Check Out Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="purpose">
                                        <option>Select Purpose</option>
                                        <option>Purpose 1</option>
                                        <option>Purpose 2</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select class="form-control" name="room">
                                        <option>Select Room</option>
                                        <option>Single Room</option>
                                        <option>Double Room</option>
                                        <option>Deluxe Room</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="no_of_person" placeholder="No of Participant/Person">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                    <button type="button" class="btn btn-info btn-custom">Search</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Main header start -->
    <header class="main-header main-header-4">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="index.html" class="logo">
                        <img src="{{asset('public/frontend')}}/img/logos/logo.png" alt="logo">
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                    <ul class="nav navbar-nav">
                        <li class="dropdown active">
                            <a href="index.html">
                                Home
                            </a>
                        </li>
                        <li class="dropdown">
                            <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                                About<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="about.html">About Us</a></li>
                                <li><a href="">Service</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                                Pages<span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="gallery-4column.html">Gallery</a>
                                </li>
                                <li><a href="booking-system.html">Booking System</a></li>
                                <li><a href="faq.html">Faq</a></li>
                                <li><a href="login.html">Login</a></li>
                                <li><a href="signup.html">Signup</a></li>
                                <li><a href="forgot-password.html">Forgot Password</a></li>
                            </ul>
                        </li>
                        <li><a href="blog-full-width.html">Blog</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right hidden-sm hidden-xs">
                        <li>
                            <a class="btn-navbar btn btn-sm btn-theme-sm-outline btn-round" data-toggle="modal" data-target="#exampleModal">Booking</a>
                        </li>
                        <li>
                            <a id="header-search-btn" class="btn-navbar search-icon"><i class="fa fa-search"></i></a>
                        </li>
                    </ul>
                </div>

                <!-- /.navbar-collapse -->
                <!-- /.container -->
            </nav>

            <div class="header-search animated fadeInDown">
                <form class="form-inline">
                    <input type="text" class="form-control" id="searchKey" placeholder="Search...">
                    <div class="search-btns">
                        <button type="submit" class="btn btn-default">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </header>
    <!-- Main header end -->

    <!-- Banner start -->
    <div class="banner banner-max-height">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="{{asset('public/frontend')}}/img/slider/slider1.jpg" alt="slider1">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content text-center">
                            <h1 data-animation="animated fadeInDown delay-05s"><span>Welcome to</span> BLC</h1>
                            <p data-animation="animated fadeInUp delay-1s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing elit <br/>Lorem ipsum dolor sit amet, conconsectetuer</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="{{asset('public/frontend')}}/img/slider/slider2.jpg" alt="slider2">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content text-center">
                            <h1 data-animation="animated fadeInLeft delay-05s"><span>Best </span>Room</h1>
                            <p data-animation="animated fadeInUp delay-05s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing elit <br/> Lorem ipsum dolor sit amet, conconsectetuer</p>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="{{asset('public/frontend')}}/img/slider/slider3.jpg" alt="slider3">
                    <div class="carousel-caption banner-slider-inner banner-top-align">
                        <div class="banner-content text-center">
                            <h1 data-animation="animated fadeInLeft delay-05s"><span>Best Place</span> for relax</h1>
                            <p data-animation="animated fadeInLeft delay-1s">Lorem ipsum dolor sit amet, conconsectetuer adipiscing <br/> elit Lorem ipsum dolor sit amet, conconsectetuer</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="slider-mover-left" aria-hidden="true">
                    <i class="fa fa-angle-left"></i>
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="slider-mover-right" aria-hidden="true">
                    <i class="fa fa-angle-right"></i>
                </span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- Search area box start -->
        <div class="search-area-box search-area-box-4 search-area-box-5">
            <div class="container">
                <div class="search-contents search-contents-3" style="background-image: url('public/frontend/img/backgnd/chk-img2.jpg');">
                    <form method="GET">
                        <div class="row search-your-details">
                            <div class="col-md-12">
                                <div class="search-your-rooms">
                                    <h4 style="color: #fff">Check Room Availibility</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <select class="selectpicker search-fields form-control-2" name="blc_name">
                                                    <option value="">Select BLC</option>
                                                    <option value="Gulshan_blc">Gulshan</option>
                                                    <option value="Rajshahi_blc">Rajshahi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <input type="text" class="btn-default datepicker" placeholder="Check In Date">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <input type="text" class="btn-default datepicker" placeholder="Check Out Date">
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <select class="selectpicker search-fields form-control-2" name="purpose">
                                                    <option>Select Purpose</option>
                                                    <option>Purpose 1</option>
                                                    <option>Purpose 2</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <select class="selectpicker search-fields form-control-2" name="room">
                                                    <option>Room</option>
                                                    <option>Single Room</option>
                                                    <option>Double Room</option>
                                                    <option>Deluxe Room</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="form-group">
                                                <input type="text" class="btn-default" name="no_of_person" placeholder="No of Participant/Person">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2" style="margin-top: 20px;">
                                    <div class="form-group">
                                        <button class="search-button btn-blocks">Search</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Search area box end -->
    </div>
    <!-- Banner end -->

    <!-- About Institute bg start -->
    <div class="about-institute-bg" style="margin: 0px 0px 0px 0px;padding: 100px 0px 0px 0px;background-image: url('public/frontend/img/backgnd/abouts6.jpg');">
        <div class="overlay">
            <div class="container">
                <!-- Main title -->
                <div class="main-title">
                    <h1>About BLC</h1>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="about-text">
                            <!-- paragraph -->
                            <p style="text-align: justify;color: #000;font-size: 15px">Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.
                            Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.</p>
                            <!-- btn -->
                            <a href="about.html" class="btn btn-info btn-custom">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="hotels-detail-slider simple-slider">
                            <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                                <div class="carousel-outer">
                                    <!-- Carousel inner -->
                                    <div class="carousel-inner">
                                        <div class="item">
                                            <img src="{{asset('public/frontend')}}/img/about/about-3.jpg" class="img-responsive" alt="about-3">
                                        </div>
                                        <div class="item active left">
                                            <img src="{{asset('public/frontend')}}/img/about/about-1.jpg" class="img-responsive" alt="about-1">
                                        </div>
                                        <div class="item next left">
                                            <img src="{{asset('public/frontend')}}/img/about/about-2.jpg" class="img-responsive" alt="about-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- About Institute bg end -->

    <!-- Partners block start -->
    <div class="partners-block" style="background: #3AC4FA;padding: 1px 0px 43px 0px;background-image: url('public/frontend/img/backgnd/feedbck.jpg');">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Our BLC</h1>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="carousel our-partners slide" id="ourPartners">
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="{{asset('public/frontend')}}/img/blc/barishal.jpg" alt="audiojungle">
                                        <p class="our-blc-para">BLC Barishal</p>
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="{{asset('public/frontend')}}/img/blc/barguna.jpg" alt="audiojungle">
                                        <p class="our-blc-para">BLC Bagura</p> 
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="{{asset('public/frontend')}}/img/blc/coxbz.jpg" alt="audiojungle">
                                        <p class="our-blc-para">BLC Cox's Bazar</p>
                                    </a>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-xs-12 col-sm-6 col-md-3">
                                    <a href="#">
                                        <img src="{{asset('public/frontend')}}/img/blc/cttg.jpg" alt="audiojungle">
                                        <p class="our-blc-para">BLC Chattogram</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                        <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Partners block end -->

    <!-- Hotel section start -->
    <div class="content-area hotel-section chevron-icon" style="background: #DDDDDD;background-image: url('public/frontend/img/backgnd/blc-backg.jpg');">
        <div class="overlay">
            <div class="container">
                <!-- Main title -->
                <div class="main-title">
                    <h1>Our Rooms</h1>
                </div>
                <div class="row">
                    <div class="carousel our-partners slide" id="ourPartners3">
                        <div class="col-lg-12 mb-30">
                            <a class="right carousel-control" href="#ourPartners3" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                            <a class="right carousel-control" href="#ourPartners3" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                        </div>
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="hotel-box">
                                        <!--header -->
                                        <div class="header clearfix">
                                            <img src="{{asset('public/frontend')}}/img/room/img-1.jpg" alt="img-1" class="img-responsive">
                                        </div>
                                        <!-- Detail -->
                                        <div class="detail clearfix">
                                            <h3>Luxury Room</h3>
                                            <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Size:</td>
                                                        <td>30 ft</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Capacity:</td>
                                                        <td>Max persion 5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bed:</td>
                                                        <td>King Beds</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services:</td>
                                                        <td>
                                                            <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                            <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                            <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="hotel-box">
                                        <!--header -->
                                        <div class="header clearfix">
                                            <img src="{{asset('public/frontend')}}/img/room/img-2.jpg" alt="img-2" class="img-responsive">
                                        </div>
                                        <!-- Detail -->
                                        <div class="detail clearfix">
                                            <h3>Single Room</h3>
                                            <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Size:</td>
                                                        <td>30 ft</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Capacity:</td>
                                                        <td>Max persion 5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bed:</td>
                                                        <td>King Beds</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services:</td>
                                                        <td>
                                                            <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                            <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                            <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="hotel-box">
                                        <!--header -->
                                        <div class="header clearfix">
                                            <img src="{{asset('public/frontend')}}/img/room/img-4.jpg" alt="img-4" class="img-responsive">
                                        </div>
                                        <!-- Detail -->
                                        <div class="detail clearfix">
                                            <h3>Family Room</h3>
                                            <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Size:</td>
                                                        <td>30 ft</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Capacity:</td>
                                                        <td>Max persion 5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bed:</td>
                                                        <td>King Beds</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services:</td>
                                                        <td>
                                                            <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                            <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                            <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="hotel-box">
                                        <!--header -->
                                        <div class="header clearfix">
                                            <img src="{{asset('public/frontend')}}/img/room/img-3.jpg" alt="img-3" class="img-responsive">
                                        </div>
                                        <!-- Detail -->
                                        <div class="detail clearfix">
                                            <h3>Double Room</h3>
                                            <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                            <table width="100%">
                                                <tbody>
                                                    <tr>
                                                        <td width="40%">Size:</td>
                                                        <td>30 ft</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Capacity:</td>
                                                        <td>Max persion 5</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bed:</td>
                                                        <td>King Beds</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Services:</td>
                                                        <td>
                                                            <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                            <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                            <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Hotel section end -->

    <!-- News and Events Start -->
    <div class="our-facilties-section content-area-7" style="background: #FFFFFF;background-image: url('public/frontend/img/backgnd/news-event.jpg');">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1 style="padding-top: 15px;">News and Events</h1>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                    <div class="list-group">
                        <a href="#" class="list-group-item active text-center">
                            <h4>New and Events Title 1</h4>
                            <p>20-04-2018</p>
                        </a>
                        <a href="#" class="list-group-item text-center">
                            <h4>New and Events Title 2</h4>
                            <p>20-05-2018</p>
                        </a>
                        <a href="#" class="list-group-item text-center">
                            <h4>New and Events Title 3</h4>
                            <p>20-06-2018</p>
                        </a>
                        <a href="#" class="list-group-item text-center">
                            <h4>New and Events Title 4</h4>
                            <p>20-06-2018</p>
                        </a>
                        <a href="#" class="list-group-item text-center">
                            <h4>New and Events Title 5</h4>
                            <p>20-04-2019</p>
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                    <!-- flight section -->
                    <div class="bhoechie-tab-content active">
                        <img src="{{asset('public/frontend')}}/img/room/img-12.jpg" alt="blog-big" class="img-responsive">
                        <h3>New and Events Title 1</h3>
                        <p>20-04-2018</p>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                    <!-- train section -->
                    <div class="bhoechie-tab-content">
                        <img src="{{asset('public/frontend')}}/img/room/img-11.jpg" alt="blog-big" class="img-responsive">
                        <h3>New and Events Title 2</h3>
                        <p>20-04-2018</p>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>

                    <!-- hotel search -->
                    <div class="bhoechie-tab-content">
                        <img src="{{asset('public/frontend')}}/img/room/img-10.jpg" alt="blog-big" class="img-responsive">
                        <h3>New and Events Title 3</h3>
                        <p>20-04-2018</p>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                    <div class="bhoechie-tab-content">
                        <img src="{{asset('public/frontend')}}/img/room/img-9.jpg" alt="blog-big" class="img-responsive">
                        <h3>New and Events Title 4</h3>
                        <p>20-04-2018</p>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                    <div class="bhoechie-tab-content">
                        <img src="{{asset('public/frontend')}}/img/room/img-8.jpg" alt="blog-big" class="img-responsive">
                        <h3>New and Events Title 5</h3>
                        <p>20-04-2019</p>
                        <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                        <a href="#" class="btn btn-info btn-custom">Read more..</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- News and Events End -->

    <!-- News and Events Start -->
    <div class="our-facilties-section content-area-7" style="background: #DDDDDD;background-image: url('public/frontend/img/backgnd/facility-cervice.jpg');">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1 style="padding-top: 15px;">Services and Facilities</h1>
            </div>
            <div class="row">
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/1.png" alt="card image"></p>
                                        <h4 class="card-title">24-hour Reception</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">24-hour Reception</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/2.png" alt="card image"></p>
                                        <h4 class="card-title">Room Service</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Room Service</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/3.png" alt="card image"></p>
                                        <h4 class="card-title">Flat Screen TV</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Flat Screen TV</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/4.png" alt="card image"></p>
                                        <h4 class="card-title">Gym</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Gym</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/5.png" alt="card image"></p>
                                        <h4 class="card-title">Free Parking</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Free Parking</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->
                <!-- Team member -->
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                        <div class="mainflip">
                            <div class="frontside">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p><img class=" img-fluid" src="{{asset('public/frontend')}}/img/facility/6.png" alt="card image"></p>
                                        <h4 class="card-title">Free Wi-Fi</h4>
                                        <p class="card-text" style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-info btn-custom"><i class="fa fa-plus" style="color: #fff;"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="backside">
                                <div class="card">
                                    <div class="card-body text-center mt-4">
                                        <h4 class="card-title">Free Wi-Fi</h4>
                                        <p class="card-text" style="text-align: justify;padding: 0px 5px 0px 5px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text everLorem</p>
                                        <a href="#" class="btn btn-primary btn-custom">Read More.</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ./Team member -->

            </div>
        </div>
    </div>
    <!-- News and Events End -->

    <!-- Blog section start -->
    <div class="blog-section content-area" style="background-image: url('public/frontend/img/backgnd/fooods.jpg');">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1 style="color: #fff;">Food Itmes</h1>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-1">
                        <div class="blog-photo">
                            <img src="{{asset('public/frontend')}}/img/food/food-1.jpg" alt="food-1" class="img-responsive">
                            <div class="profile-user">
                                250 TK <br/>
                                ONLY
                            </div>
                        </div>
                        <div class="detail">
                            <h3 style="color: #fff;">Fish and Rice</h3>
                            <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                            <a href="#" class="btn btn-info btn-custom">Read more..</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 hidden-sm">
                    <div class="blog-1">
                        <div class="blog-photo">
                            <img src="{{asset('public/frontend')}}/img/food/food-2.jpg" alt="food-2" class="img-responsive">
                            <div class="profile-user">
                                350 TK <br/>
                                ONLY
                            </div>
                        </div>
                        <div class="detail">
                            <h3 style="color: #fff;">Tehari and Biryani</h3>
                            <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                            <a href="#" class="btn btn-info btn-custom">Read more..</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="blog-1">
                        <div class="blog-photo">
                            <img src="{{asset('public/frontend')}}/img/food/food-3.jpg" alt="food-3" class="img-responsive">
                            <div class="profile-user">
                                300 TK <br/>
                                ONLY
                            </div>
                        </div>
                        <div class="detail">
                            <h3 style="color: #fff;">Chili Chicken</h3>
                            <p style="text-align: justify;color: #fff;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</p>
                            <a href="#" class="btn btn-info btn-custom">Read more..</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Blog section end -->

    <!-- Gallery secion start -->
    <div class="gallery-secion content-area-11" style="padding-top: 20px;background-image: url('public/frontend/img/backgnd/gallery-bcknd.jpg');">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1 style="color: #fff;">Our Gallery</h1>
            </div>
            <ul class="list-inline-listing filters filters-listing-navigation">
                <li class="active btn filtr-button filtr" data-filter="all">All</li>
                <li data-filter="1" class="btn btn-inline filtr-button filtr">Classic</li>
                <li data-filter="2" class="btn btn-inline filtr-button filtr">Deluxe</li>
                <li data-filter="3" class="btn btn-inline filtr-button filtr">Royal</li>
                <li data-filter="4" class="btn btn-inline filtr-button filtr">Luxury</li>
            </ul>
            <div class="row">
                <div class="filtr-container">

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="3, 2, 4">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-3.jpg" alt="img-3" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Luxury Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="3, 4, 1">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-2.jpg" alt="img-2" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Double Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 4, 2">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-1.jpg" alt="img-1" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Single Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="2, 3, 1">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-4.jpg" alt="img-4" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Family Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 2, 3">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-5.jpg" alt="img-5" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Standard</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="4, 2, 1">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-6.jpg" alt="img-6" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Couple Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="1, 4, 2">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-7.jpg" alt="img-7" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Single Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12  filtr-item" data-category="2, 3, 4">
                        <figure class="portofolio-thumb">
                            <a href="#"><img src="{{asset('public/frontend')}}/img/room/img-15-2.jpg" alt="img-8" class="img-responsive"></a>
                            <figcaption>
                                <div class="figure-content">
                                    <h3 class="title">Family Room</h3>
                                </div>
                            </figcaption>
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Gallery secion end -->

    <!-- Search area box 2 start -->
    <div class="search-area-box-2 search-area-box-6" style="background-image: url('public/frontend/img/backgnd/feedbck.jpg');">
        <div class="container">
            <div class="search-contents">
                <form>
                    <div class="row search-your-details">
                        <div class="col-lg-3 col-md-3">
                            <div class="search-your-rooms mt-20">
                                <h2 class="hidden-xs hidden-sm">Your <span style="color: #EC008C;">Feedback</span></h2>
                                <h2 class="hidden-lg hidden-md">Your <span style="color: #EC008C;">Feedback</span></h2>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="row">
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields form-control-2" name="blc">
                                            <option>Select BLC</option>
                                            <option>Rajshahi</option>
                                            <option>Gulshan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input type="text" class="btn-default" placeholder="Write Mobile No">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input type="email" class="btn-default" placeholder="Write Email Address">
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-6">
                                    <div class="form-group">
                                        <textarea name="feedback" class="btn-default" placeholder="Write Feedback" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-6">
                                    <div class="form-group">
                                        <button class="btn btn-info btn-custom">Send Feedback</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Search area box 2 end -->

    <!-- Footer start -->
    <footer class="main-footer clearfix">
        <div class="container">
            <!-- Footer info-->
            <div class="footer-info">
                <div class="row">
                    <!-- About us -->
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="footer-item">
                            <div class="footer-logo">
                                <a href="index.html">
                                    <img src="{{asset('public/frontend')}}/img/logos/logo.png" alt="white-logo">
                                </a>
                            </div>
                            <p>Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam.</p>
                        </div>
                    </div>
                    <!-- Contact Us -->
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>CONTACT US</h1>
                            </div>
                            <ul class="personal-info">
                                <li>
                                    <i class="fa fa-map-marker"></i>
                                    <p>Address: BRAC Centre, 75 Mohakhali, Dhaka-1212.</p>
                                </li>
                                <li>
                                    <i class="fa fa-envelope"></i>
                                    <p>Email: info@brac.net</p>
                                </li>
                                <li>
                                    <i class="fa fa-phone"></i>
                                    <p>Phone: 880-2-9881265</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Contact Us -->
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="footer-item">
                            <div class="main-title-2">
                                <h1>JOIN WITH US</h1>
                            </div>
                            <div class="clearfix"></div>
                            <ul class="social-list">
                                <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="rss-bg"><i class="fa fa-rss"></i></a></li>
                                <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Copy right start -->
    <div class="copy-right">
        <div class="container">
            Copyright &copy;  2020 brac. designed and developed by <a href="">Nanosoft</a>
        </div>
    </div>
    <!-- Copy end right-->

    <script  src="{{asset('public/frontend')}}/js/jquery-2.2.0.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-submenu.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.mb.YTPlayer.js"></script>
    <script  src="{{asset('public/frontend')}}/js/wow.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-select.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.easing.1.3.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.scrollUp.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/jquery.filterizr.js"></script>
    <script  src="{{asset('public/frontend')}}/js/bootstrap-datepicker.min.js"></script>
    <script  src="{{asset('public/frontend')}}/js/app.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script  src="{{asset('public/frontend')}}/js/ie10-viewport-bug-workaround.js"></script>
    <!-- Custom javascript -->

    <!-- Position fixed Script Start -->
    <script type="text/javascript">
        $(window).on('scroll',function(){
            var scroll = $(window).scrollTop();
            if(scroll>41){
                $('.main-header').addClass('fix-pos');
            }else{
                $('.main-header').removeClass('fix-pos');

            }
            console.log(scroll);

        });
    </script>
    <!-- Position fixed Script End -->
    <script type="text/javascript">
        $(document).ready(function() {
            $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
                e.preventDefault();
                $(this).siblings('a.active').removeClass("active");
                $(this).addClass("active");
                var index = $(this).index();
                $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
                $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
            });
        });
    </script>
</body>
</html>