<!-- News and Events Start -->
<div class="our-facilties-section content-area-7" style="background: #FFFFFF;background-image: url('public/frontend/img/backgnd/news-event.jpg');">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1 style="padding-top: 15px;">News and Events</h1>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <div class="list-group">
                    <a href="#" class="list-group-item active text-center">
                        <h4>New and Events Title 1</h4>
                        <p>20-04-2018</p>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <h4>New and Events Title 2</h4>
                        <p>20-05-2018</p>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <h4>New and Events Title 3</h4>
                        <p>20-06-2018</p>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <h4>New and Events Title 4</h4>
                        <p>20-06-2018</p>
                    </a>
                    <a href="#" class="list-group-item text-center">
                        <h4>New and Events Title 5</h4>
                        <p>20-04-2019</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active" style="padding">
                    <img src="{{asset('public/frontend')}}/img/room/img-12.jpg" alt="blog-big" class="img-responsive">
                    <h3>New and Events Title 1</h3>
                    <p>20-04-2018</p>
                    <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                    <a href="#" class="btn btn-info btn-custom">Read more..</a>
                </div>
                <!-- train section -->
                <div class="bhoechie-tab-content">
                    <img src="{{asset('public/frontend')}}/img/room/img-11.jpg" alt="blog-big" class="img-responsive">
                    <h3>New and Events Title 2</h3>
                    <p>20-04-2018</p>
                    <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                    <a href="#" class="btn btn-info btn-custom">Read more..</a>
                </div>

                <!-- hotel search -->
                <div class="bhoechie-tab-content">
                    <img src="{{asset('public/frontend')}}/img/room/img-10.jpg" alt="blog-big" class="img-responsive">
                    <h3>New and Events Title 3</h3>
                    <p>20-04-2018</p>
                    <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                    <a href="#" class="btn btn-info btn-custom">Read more..</a>
                </div>
                <div class="bhoechie-tab-content">
                    <img src="{{asset('public/frontend')}}/img/room/img-9.jpg" alt="blog-big" class="img-responsive">
                    <h3>New and Events Title 4</h3>
                    <p>20-04-2018</p>
                    <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                    <a href="#" class="btn btn-info btn-custom">Read more..</a>
                </div>
                <div class="bhoechie-tab-content">
                    <img src="{{asset('public/frontend')}}/img/room/img-8.jpg" alt="blog-big" class="img-responsive">
                    <h3>New and Events Title 5</h3>
                    <p>20-04-2019</p>
                    <p style="text-align: justify;">Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat,</p>
                    <a href="#" class="btn btn-info btn-custom">Read more..</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- News and Events End -->