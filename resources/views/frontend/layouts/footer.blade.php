<!-- Footer start -->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer info-->
        <div class="footer-info">
            <div class="row">
                <!-- About us -->
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="footer-item">
                        <div class="footer-logo">
                            <a href="index.html">
                                <img src="{{asset('public/frontend')}}/img/logos/logo.png" alt="white-logo">
                            </a>
                        </div>
                        <p>Lorem ipsum dolor sit amet, conser adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a conser nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam.</p>
                    </div>
                </div>
                <!-- Contact Us -->
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>CONTACT US</h1>
                        </div>
                        <ul class="personal-info">
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <p>Address: BRAC Centre, 75 Mohakhali, Dhaka-1212.</p>
                            </li>
                            <li>
                                <i class="fa fa-envelope"></i>
                                <p>Email: info@brac.net</p>
                            </li>
                            <li>
                                <i class="fa fa-phone"></i>
                                <p>Phone: 880-2-9881265</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Contact Us -->
                <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>JOIN WITH US</h1>
                        </div>
                        <div class="clearfix"></div>
                        <ul class="social-list">
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="rss-bg"><i class="fa fa-rss"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
        Copyright &copy;  2020 brac. designed and developed by <a href="">Nanosoft</a>
    </div>
</div>
<!-- Copy end right-->