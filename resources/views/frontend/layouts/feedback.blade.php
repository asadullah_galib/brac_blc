<!-- Feedback Search area box 2 start -->
<div class="search-area-box-2 search-area-box-6" style="background-image: url('public/frontend/img/backgnd/feedbck.jpg');">
    <div class="container">
        <div class="search-contents">
            <form>
                <div class="row search-your-details">
                    <div class="col-lg-3 col-md-3">
                        <div class="search-your-rooms mt-20">
                            <h2 class="hidden-xs hidden-sm">Your <span style="color: #EC008C;">Feedback</span></h2>
                            <h2 class="hidden-lg hidden-md">Your <span style="color: #EC008C;">Feedback</span></h2>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <select class="selectpicker search-fields form-control-2" name="blc">
                                        <option>Select BLC</option>
                                        <option>Rajshahi</option>
                                        <option>Gulshan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="text" class="btn-default" placeholder="Write Mobile No">
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <input type="email" class="btn-default" placeholder="Write Email Address">
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-6">
                                <div class="form-group">
                                    <textarea name="feedback" class="btn-default" placeholder="Write Feedback" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-6">
                                <div class="form-group">
                                    <button class="btn btn-info btn-custom">Send Feedback</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Feedback Search area box 2 end -->