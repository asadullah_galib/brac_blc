<!-- Our Room Hotel section start -->
<div class="content-area hotel-section chevron-icon" style="background: #DDDDDD;background-image: url('public/frontend/img/backgnd/blc-backg.jpg');">
    <div class="overlay">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>Our Rooms</h1>
            </div>
            <div class="row">
                <div class="carousel our-partners slide" id="ourPartners3">
                    <div class="col-lg-12 mb-30">
                        <a class="right carousel-control" href="#ourPartners3" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                        <a class="right carousel-control" href="#ourPartners3" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                    </div>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="hotel-box">
                                    <!--header -->
                                    <div class="header clearfix">
                                        <img src="{{asset('public/frontend')}}/img/room/img-1.jpg" alt="img-1" class="img-responsive">
                                    </div>
                                    <!-- Detail -->
                                    <div class="detail clearfix">
                                        <h3>Luxury Room</h3>
                                        <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="40%">Size:</td>
                                                    <td>30 ft</td>
                                                </tr>
                                                <tr>
                                                    <td>Capacity:</td>
                                                    <td>Max persion 5</td>
                                                </tr>
                                                <tr>
                                                    <td>Bed:</td>
                                                    <td>King Beds</td>
                                                </tr>
                                                <tr>
                                                    <td>Services:</td>
                                                    <td>
                                                        <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                        <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                        <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="hotel-box">
                                    <!--header -->
                                    <div class="header clearfix">
                                        <img src="{{asset('public/frontend')}}/img/room/img-2.jpg" alt="img-2" class="img-responsive">
                                    </div>
                                    <!-- Detail -->
                                    <div class="detail clearfix">
                                        <h3>Single Room</h3>
                                        <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="40%">Size:</td>
                                                    <td>30 ft</td>
                                                </tr>
                                                <tr>
                                                    <td>Capacity:</td>
                                                    <td>Max persion 5</td>
                                                </tr>
                                                <tr>
                                                    <td>Bed:</td>
                                                    <td>King Beds</td>
                                                </tr>
                                                <tr>
                                                    <td>Services:</td>
                                                    <td>
                                                        <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                        <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                        <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="hotel-box">
                                    <!--header -->
                                    <div class="header clearfix">
                                        <img src="{{asset('public/frontend')}}/img/room/img-4.jpg" alt="img-4" class="img-responsive">
                                    </div>
                                    <!-- Detail -->
                                    <div class="detail clearfix">
                                        <h3>Family Room</h3>
                                        <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="40%">Size:</td>
                                                    <td>30 ft</td>
                                                </tr>
                                                <tr>
                                                    <td>Capacity:</td>
                                                    <td>Max persion 5</td>
                                                </tr>
                                                <tr>
                                                    <td>Bed:</td>
                                                    <td>King Beds</td>
                                                </tr>
                                                <tr>
                                                    <td>Services:</td>
                                                    <td>
                                                        <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                        <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                        <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="hotel-box">
                                    <!--header -->
                                    <div class="header clearfix">
                                        <img src="{{asset('public/frontend')}}/img/room/img-3.jpg" alt="img-3" class="img-responsive">
                                    </div>
                                    <!-- Detail -->
                                    <div class="detail clearfix">
                                        <h3>Double Room</h3>
                                        <p style="color: #EC008C; font-weight: bold;">$999/pernight</p>
                                        <table width="100%">
                                            <tbody>
                                                <tr>
                                                    <td width="40%">Size:</td>
                                                    <td>30 ft</td>
                                                </tr>
                                                <tr>
                                                    <td>Capacity:</td>
                                                    <td>Max persion 5</td>
                                                </tr>
                                                <tr>
                                                    <td>Bed:</td>
                                                    <td>King Beds</td>
                                                </tr>
                                                <tr>
                                                    <td>Services:</td>
                                                    <td>
                                                        <i class="fa fa-wifi" style="font-size: 17px;"></i> |  <i class="fa fa-clock-o" style="font-size: 17px;"></i> |
                                                        <i class="fa fa-laptop" style="font-size: 17px;"></i> | 
                                                        <i class="fa fa-umbrella" style="font-size: 17px;"></i>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" class="text-center"><a class="btn btn-info btn-custom" href="rooms-details.html">Read More</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Our Room Hotel section end -->