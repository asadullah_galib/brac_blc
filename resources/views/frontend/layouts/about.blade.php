<!-- About Institute bg start -->
<div class="about-institute-bg" style="margin: 0px 0px 0px 0px;padding: 100px 0px 0px 0px;background-image: url('public/frontend/img/backgnd/abouts6.jpg');">
    <div class="overlay">
        <div class="container">
            <!-- Main title -->
            <div class="main-title">
                <h1>About BLC</h1>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="about-text">
                        <!-- paragraph -->
                        <p style="text-align: justify;color: #000;font-size: 15px">Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.
                        Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.Lorem ipsum dolor sit amet, conser adipiscing elit. Sed tempor arcu non ligula convallis, vel tincidunt ipsum posuere. Fusce sodales lacus ut pellentes sollicitudin.Duis iaculis, arcu ut hendrerit pharetra.</p>
                        <!-- btn -->
                        <a href="about.html" class="btn btn-info btn-custom">Read More</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="hotels-detail-slider simple-slider">
                        <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                            <div class="carousel-outer">
                                <!-- Carousel inner -->
                                <div class="carousel-inner">
                                    <div class="item">
                                        <img src="{{asset('public/frontend')}}/img/about/about-3.jpg" class="img-responsive" alt="about-3">
                                    </div>
                                    <div class="item active left">
                                        <img src="{{asset('public/frontend')}}/img/about/about-1.jpg" class="img-responsive" alt="about-1">
                                    </div>
                                    <div class="item next left">
                                        <img src="{{asset('public/frontend')}}/img/about/about-2.jpg" class="img-responsive" alt="about-2">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About Institute bg end -->