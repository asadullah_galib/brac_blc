-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 07, 2020 at 05:35 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `brac_blc`
--

-- --------------------------------------------------------

--
-- Table structure for table `blc_categories`
--

DROP TABLE IF EXISTS `blc_categories`;
CREATE TABLE IF NOT EXISTS `blc_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blc_categories`
--

INSERT INTO `blc_categories` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Category 1', 1, 1, 1, '2020-04-20 00:34:37', '2020-04-22 05:55:55'),
(2, 'Category 2', 1, 1, 1, '2020-04-20 00:38:02', '2020-04-21 04:34:33'),
(3, 'Category 3', 1, 1, 1, '2020-04-21 04:34:52', '2020-04-22 06:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `blc_names`
--

DROP TABLE IF EXISTS `blc_names`;
CREATE TABLE IF NOT EXISTS `blc_names` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `blc_category_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blc_names`
--

INSERT INTO `blc_names` (`id`, `blc_category_id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 2, 'Gulshan BLC', 1, 1, 1, '2020-04-20 00:48:27', '2020-04-20 00:49:53'),
(2, 1, 'Test BLC Name', 1, 1, NULL, '2020-04-20 01:00:25', '2020-04-20 01:00:25');

-- --------------------------------------------------------

--
-- Table structure for table `icons`
--

DROP TABLE IF EXISTS `icons`;
CREATE TABLE IF NOT EXISTS `icons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'N/A',
  `deleted_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `icons`
--

INSERT INTO `icons` (`id`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'fa-copy', NULL, '2018-06-07 08:58:42', '2018-06-10 05:36:58'),
(2, 'ion-social-twitter', NULL, '2018-06-25 09:52:14', '2018-10-21 03:04:51'),
(12, 'ion-ionic', NULL, '2018-10-21 03:04:18', '2018-10-21 03:04:18'),
(13, 'ion-settings', NULL, '2018-11-15 01:00:22', '2018-11-15 01:00:22'),
(14, 'ion-person-stalker', NULL, '2018-11-15 05:08:11', '2018-11-15 05:08:11'),
(15, 'ion-cash', NULL, '2018-11-28 06:16:02', '2018-11-28 06:16:02'),
(16, 'ion-model-s', NULL, '2019-02-04 07:03:00', '2019-02-04 07:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT 0,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT 0,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `parent`, `route`, `sort`, `status`, `icon`, `created_at`, `updated_at`) VALUES
(1, 'Menu Management', 0, 'menu', 1, 1, '', '2018-05-16 03:20:43', '2018-12-31 21:16:50'),
(2, 'Menu List', 1, 'menu', 11, 1, '', '2018-05-16 04:03:04', '2018-05-16 04:03:04'),
(3, 'Menu Icon', 1, 'menu.icon', 12, 1, '', '2018-06-06 04:35:13', '2018-06-06 08:31:29'),
(4, 'Use Management', 0, 'user', 2, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(5, 'User Role', 4, 'user.role.view', 2, 1, '', '2018-06-09 04:57:27', '2019-08-05 04:55:35'),
(6, 'Menu Permission', 4, 'user.permission', 5, 1, '', '2018-06-05 06:59:51', '2019-08-05 04:56:15'),
(7, 'Master Setup', 0, 'setup', 4, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(8, 'BLC Category', 7, 'setup.blc.category.view', 1, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(9, 'BLC Name', 7, 'setup.blc.name.view', 2, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(10, 'BLC Room Categories', 7, 'setup.blc.room-category.view', 3, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(11, 'BLC Room Facilities', 7, 'setup.blc.room-facilities.view', 4, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(12, 'BLC Room Type', 7, 'setup.blc.room-type.view', 5, 1, '', '2018-06-09 04:29:53', '2019-08-05 04:54:46'),
(13, 'User Role Type', 4, 'user.role.type.view', 1, 1, '', '2018-06-05 06:59:51', '2019-08-05 04:56:15'),
(14, 'User List', 4, 'user.view', 3, 1, '', '2018-06-05 06:59:51', '2019-08-05 04:56:15'),
(15, 'BLC Based User', 4, 'user.blc.view', 4, 1, '', '2018-06-05 06:59:51', '2019-08-05 04:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `menu_permissions`
--

DROP TABLE IF EXISTS `menu_permissions`;
CREATE TABLE IF NOT EXISTS `menu_permissions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permitted_route` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_from` varchar(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1091 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu_permissions`
--

INSERT INTO `menu_permissions` (`id`, `menu_id`, `role_id`, `permitted_route`, `menu_from`, `created_at`, `updated_at`) VALUES
(9, 5, 5, 'user.role', 'menu', '2019-11-05 22:51:45', '2019-11-05 22:51:45'),
(10, 4, 5, 'user', 'menu', '2019-11-05 22:51:45', '2019-11-05 22:51:45'),
(11, 6, 5, 'user.permission', 'menu', '2019-11-05 22:51:45', '2019-11-05 22:51:45'),
(13, 10, 5, 'project-management.person.project.view', 'menu', '2019-11-05 22:51:45', '2019-11-05 22:51:45'),
(14, 7, 8, 'project-management', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(15, 9, 8, 'project-management.resource.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(16, 12, 8, 'project-management.person.project.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(17, 13, 8, 'project-management.project.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(18, 14, 8, 'project-management.contact.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(19, 15, 8, 'project-management.contribution.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(20, 16, 8, 'project-management.role.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(21, 17, 8, 'project-management.evaluation.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(22, 18, 8, 'project-management.meeting.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(23, 19, 8, 'project-management.task.assign.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(24, 8, 8, 'project-management.client.view', 'menu', '2019-11-06 00:08:53', '2019-11-06 00:08:53'),
(28, 7, 5, 'project-management', 'menu', '2019-11-06 00:12:19', '2019-11-06 00:12:19'),
(29, 17, 5, 'project-management.evaluation.view', 'menu', '2019-11-06 00:12:19', '2019-11-06 00:12:19'),
(30, 19, 5, 'project-management.task.assign.view', 'menu', '2019-11-06 00:12:19', '2019-11-06 00:12:19'),
(43, 13, 7, 'project-management.project.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(44, 7, 7, 'project-management', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(45, 14, 7, 'project-management.contact.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(46, 19, 7, 'project-management.task.assign.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(47, 17, 7, 'project-management.evaluation.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(48, 18, 7, 'project-management.meeting.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(49, 12, 7, 'project-management.person.project.view', 'menu', '2019-11-06 21:38:13', '2019-11-06 21:38:13'),
(54, 15, 4, 'project-management.contribution.view', 'menu', '2019-11-06 21:44:41', '2019-11-06 21:44:41'),
(55, 7, 4, 'project-management', 'menu', '2019-11-06 21:44:41', '2019-11-06 21:44:41'),
(56, 19, 4, 'project-management.task.assign.view', 'menu', '2019-11-06 21:44:41', '2019-11-06 21:44:41'),
(955, 9, 21, 'project-management.resource.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(956, 7, 21, 'project-management', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(962, 18, 21, 'project-management.meeting.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(963, 19, 21, 'project-management.task.assign.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(964, 14, 21, 'project-management.task.assign.action', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(965, 15, 21, 'project-management.task.assign.view-button', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(966, 16, 21, 'project-management.task.assign.save-button', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(967, 15, 21, 'project-management.contribution.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(968, 19, 21, 'project-management.contribution.resource_person', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(969, 17, 21, 'project-management.evaluation.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(970, 9, 21, 'project-management.evaluation.view.working-hour', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(971, 10, 21, 'project-management.evaluation.view.expected-time', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(972, 11, 21, 'project-management.evaluation.view.action', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(973, 12, 21, 'project-management.evaluation.view.button', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(974, 13, 21, 'project-management.evaluation.view.upddate-button', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(975, 17, 21, 'project-management.evaluation.view.start_time', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(976, 18, 21, 'project-management.evaluation.view.end_time', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(977, 20, 21, 'project-management.evaluation.resources', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(978, 20, 21, 'project-management.resource.profile.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(979, 21, 21, 'project-management.all.resource.view', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(980, 2, 21, 'project-management.resource.add-button', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(981, 3, 21, 'project-management.resource.present-address', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(982, 4, 21, 'project-management.resource.permanent-address', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(983, 5, 21, 'project-management.resource.designation', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(984, 6, 21, 'project-management.resource.contact', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(985, 7, 21, 'project-management.resource.action', 'menu_route', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(986, 22, 21, 'project-management.change.password', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(987, 23, 21, 'project-management.man_month.report', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(988, 25, 21, 'project-management.attendance.resource', 'menu', '2020-01-03 01:36:35', '2020-01-03 01:36:35'),
(1006, 22, 19, 'project-management.change.password', 'menu', '2020-01-03 01:36:57', '2020-01-03 01:36:57'),
(1010, 7, 18, 'project-management', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1014, 18, 18, 'project-management.meeting.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1015, 19, 18, 'project-management.task.assign.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1016, 14, 18, 'project-management.task.assign.action', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1017, 16, 18, 'project-management.task.assign.save-button', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1018, 15, 18, 'project-management.contribution.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1019, 19, 18, 'project-management.contribution.resource_person', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1020, 17, 18, 'project-management.evaluation.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1021, 10, 18, 'project-management.evaluation.view.expected-time', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1022, 11, 18, 'project-management.evaluation.view.action', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1023, 13, 18, 'project-management.evaluation.view.upddate-button', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1024, 20, 18, 'project-management.evaluation.resources', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1025, 24, 18, 'project-management.own.evaluation', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1026, 20, 18, 'project-management.resource.profile.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1027, 21, 18, 'project-management.all.resource.view', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1028, 3, 18, 'project-management.resource.present-address', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1029, 4, 18, 'project-management.resource.permanent-address', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1030, 5, 18, 'project-management.resource.designation', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1031, 6, 18, 'project-management.resource.contact', 'menu_route', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1032, 22, 18, 'project-management.change.password', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1033, 23, 18, 'project-management.man_month.report', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1034, 25, 18, 'project-management.attendance.resource', 'menu', '2020-01-03 01:37:15', '2020-01-03 01:37:15'),
(1035, 8, 22, 'project-management.client.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1036, 7, 22, 'project-management', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1037, 13, 22, 'project-management.project.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1038, 14, 22, 'project-management.contact.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1039, 19, 22, 'project-management.task.assign.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1040, 14, 22, 'project-management.task.assign.action', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1041, 15, 22, 'project-management.task.assign.view-button', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1042, 16, 22, 'project-management.task.assign.save-button', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1043, 15, 22, 'project-management.contribution.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1044, 17, 22, 'project-management.evaluation.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1045, 10, 22, 'project-management.evaluation.view.expected-time', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1046, 12, 22, 'project-management.evaluation.view.button', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1047, 20, 22, 'project-management.resource.profile.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1048, 21, 22, 'project-management.all.resource.view', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1049, 3, 22, 'project-management.resource.present-address', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1050, 4, 22, 'project-management.resource.permanent-address', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1051, 5, 22, 'project-management.resource.designation', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1052, 6, 22, 'project-management.resource.contact', 'menu_route', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1053, 22, 22, 'project-management.change.password', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1054, 23, 22, 'project-management.man_month.report', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1055, 25, 22, 'project-management.attendance.resource', 'menu', '2020-01-03 01:37:37', '2020-01-03 01:37:37'),
(1056, 4, 21, 'user', 'menu', '2020-01-05 15:35:40', '2020-01-05 15:35:40'),
(1057, 5, 21, 'user.role', 'menu', '2020-01-05 15:35:40', '2020-01-05 15:35:40'),
(1058, 6, 21, 'user.permission', 'menu', '2020-01-05 15:35:40', '2020-01-05 15:35:40'),
(1059, 8, 21, 'setup-management.client.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1060, 31, 21, 'setup-management', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1061, 13, 21, 'setup-management.project.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1062, 14, 21, 'setup-management.contact.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1063, 16, 21, 'setup-management.role.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1064, 12, 21, 'setup-management.person.project.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1065, 32, 21, 'setup-management.resource.view', 'menu', '2020-01-05 15:36:24', '2020-01-05 15:36:24'),
(1066, 8, 18, 'setup-management.client.view', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1067, 31, 18, 'setup-management', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1068, 13, 18, 'setup-management.project.view', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1069, 14, 18, 'setup-management.contact.view', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1070, 12, 18, 'setup-management.person.project.view', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1071, 32, 18, 'setup-management.resource.view', 'menu', '2020-01-05 15:36:46', '2020-01-05 15:36:46'),
(1072, 19, 19, 'project-management.task.assign.view', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1073, 7, 19, 'project-management', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1074, 14, 19, 'project-management.task.assign.action', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1075, 16, 19, 'project-management.task.assign.save-button', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1076, 15, 19, 'project-management.contribution.view', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1077, 17, 19, 'project-management.evaluation.view', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1078, 9, 19, 'project-management.evaluation.view.working-hour', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1079, 10, 19, 'project-management.evaluation.view.expected-time', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1080, 12, 19, 'project-management.evaluation.view.button', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1081, 17, 19, 'project-management.evaluation.view.start_time', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1082, 18, 19, 'project-management.evaluation.view.end_time', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1083, 20, 19, 'project-management.resource.profile.view', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1084, 21, 19, 'project-management.all.resource.view', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1085, 3, 19, 'project-management.resource.present-address', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1086, 4, 19, 'project-management.resource.permanent-address', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1087, 5, 19, 'project-management.resource.designation', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1088, 6, 19, 'project-management.resource.contact', 'menu_route', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1089, 25, 19, 'project-management.attendance.resource', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49'),
(1090, 23, 19, 'project-management.man_month.report', 'menu', '2020-01-20 14:58:49', '2020-01-20 14:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu_routes`
--

DROP TABLE IF EXISTS `menu_routes`;
CREATE TABLE IF NOT EXISTS `menu_routes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_routes`
--

INSERT INTO `menu_routes` (`id`, `menu_id`, `name`, `sort`, `route`, `status`, `created_at`, `updated_at`) VALUES
(2, 21, 'add_button', NULL, 'project-management.resource.add-button', 1, NULL, NULL),
(3, 21, 'present_address', NULL, 'project-management.resource.present-address', 1, NULL, NULL),
(4, 21, 'permanent_address', NULL, 'project-management.resource.permanent-address', 1, NULL, NULL),
(5, 21, 'designation', NULL, 'project-management.resource.designation', 1, NULL, NULL),
(6, 21, 'contact', NULL, 'project-management.resource.contact', 1, NULL, NULL),
(7, 21, 'action', NULL, 'project-management.resource.action', 1, NULL, NULL),
(9, 17, 'working_hour', NULL, 'project-management.evaluation.view.working-hour', 1, NULL, NULL),
(10, 17, 'expected_time', NULL, 'project-management.evaluation.view.expected-time', 1, NULL, NULL),
(11, 17, 'Action', NULL, 'project-management.evaluation.view.action', 1, NULL, NULL),
(12, 17, 'View_Button', NULL, 'project-management.evaluation.view.button', 1, NULL, NULL),
(13, 17, 'Update_Button', NULL, 'project-management.evaluation.view.upddate-button', 1, NULL, NULL),
(14, 19, 'Action', NULL, 'project-management.task.assign.action', 1, NULL, NULL),
(15, 19, 'View_Button', NULL, 'project-management.task.assign.view-button', 1, NULL, NULL),
(16, 19, 'save-update-button', NULL, 'project-management.task.assign.save-button', 1, NULL, NULL),
(17, 17, 'Start Time', NULL, 'project-management.evaluation.view.start_time', 1, NULL, NULL),
(18, 17, 'End Time', NULL, 'project-management.evaluation.view.end_time', 1, NULL, NULL),
(19, 15, 'Resource Person Field', NULL, 'project-management.contribution.resource_person', 1, NULL, NULL),
(20, 17, 'Resource Person field', NULL, 'project-management.evaluation.resources', 1, NULL, NULL),
(21, 24, 'update_button', NULL, 'project-management.evaluation.own.update-button', 1, NULL, NULL),
(22, 24, 'view_button', NULL, 'project-management.evaluation.own.view-button', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_01_090751_create_site_settings_table', 2),
(4, '2019_08_05_043423_create_sliders_table', 1),
(9, '2019_10_23_063401_create_clients_table', 3),
(10, '2019_10_23_063512_create_projects_table', 3),
(46, '2019_10_23_063557_create_resource_people_table', 28),
(12, '2019_10_23_063711_create_contact_people_table', 3),
(32, '2019_10_23_063743_create_resource_contributions_table', 24),
(18, '2019_10_23_093808_create_resource_person_projects_table', 21),
(17, '2019_10_26_132331_create_roles_table', 20),
(30, '2019_10_27_034624_create_evaluations_table', 23),
(33, '2019_10_29_180039_create_task_assigns_table', 25),
(34, '2019_10_30_050003_create_task_assign_contributions_table', 25),
(43, '2019_10_30_091747_create_meetings_table', 27),
(40, '2019_10_31_040047_create_meeting_resource_people_table', 26),
(41, '2019_10_31_040208_create_meeting_other_people_table', 26),
(47, '2019_12_22_100443_add_evaluated_by_to_users_table', 29),
(48, '2020_04_19_115351_create_blc_categories_table', 30),
(49, '2020_04_19_115427_create_blc_names_table', 30),
(50, '2020_04_19_115517_create_room_categories_table', 30),
(51, '2020_04_19_115646_create_room_facilities_table', 30),
(52, '2020_04_19_115722_create_room_types_table', 30),
(53, '2020_04_20_085638_create_user_role_types_table', 31);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_role_type_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_slug` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `deleted_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `user_role_type_id`, `name`, `description`, `role_slug`, `status`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`) VALUES
(19, 1, 'Developer', NULL, 'pIAuxT', 1, NULL, NULL, 1, NULL, '2019-11-12 00:55:21', '2020-04-20 06:17:58'),
(18, 2, 'Project Manager', NULL, 'WTle2y', 1, NULL, NULL, 1, NULL, '2019-11-11 23:37:38', '2020-04-20 06:19:15'),
(21, 1, 'Admin', NULL, '6JkqPK', 1, NULL, NULL, 1, NULL, '2019-11-14 16:48:59', '2020-04-20 06:21:25'),
(22, 2, 'Tester', NULL, NULL, 1, NULL, NULL, 1, NULL, '2019-11-28 14:17:09', '2020-04-20 06:21:31'),
(23, 1, 'Test Purpose', NULL, NULL, 1, NULL, 1, NULL, NULL, '2020-04-20 06:28:00', '2020-04-20 06:28:00');

-- --------------------------------------------------------

--
-- Table structure for table `room_categories`
--

DROP TABLE IF EXISTS `room_categories`;
CREATE TABLE IF NOT EXISTS `room_categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Dormitory,guest room,etc',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_categories`
--

INSERT INTO `room_categories` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Dormitory', 1, 1, 1, '2020-04-20 01:23:54', '2020-04-20 01:25:31'),
(2, 'Guest Room', 1, 1, NULL, '2020-04-20 01:25:52', '2020-04-20 01:25:52'),
(3, 'Training room', 1, 1, NULL, '2020-04-21 04:36:02', '2020-04-21 04:36:02'),
(4, 'Hall room', 1, 1, NULL, '2020-04-21 04:36:30', '2020-04-21 04:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `room_facilities`
--

DROP TABLE IF EXISTS `room_facilities`;
CREATE TABLE IF NOT EXISTS `room_facilities` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Icon',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_facilities`
--

INSERT INTO `room_facilities` (`id`, `name`, `image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'WiFi', '202004200752Koala.jpg', 1, 1, 1, '2020-04-20 01:52:26', '2020-04-21 04:37:27'),
(2, 'TV', '202004200757Tulips.jpg', 1, 1, 1, '2020-04-20 01:57:36', '2020-04-21 04:38:06');

-- --------------------------------------------------------

--
-- Table structure for table `room_types`
--

DROP TABLE IF EXISTS `room_types`;
CREATE TABLE IF NOT EXISTS `room_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'single,double,etc',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `room_types`
--

INSERT INTO `room_types` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Single AC', 1, 1, 1, '2020-04-20 02:13:16', '2020-04-20 02:16:14'),
(2, 'Single Non-AC', 1, 1, NULL, '2020-04-20 02:16:49', '2020-04-20 02:16:49');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

DROP TABLE IF EXISTS `site_settings`;
CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `company_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_title_bn` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_short_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_short_description_bn` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_header_logo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_footer_logo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_favicon` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_banner_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_phone_primary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_phone_secondary` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_driver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_encryption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `linkedin_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pinterest_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tumblr_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flickr_url` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recaptcha_key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `recaptcha_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus_key` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_plus_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_map_api` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_width` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_height` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_size` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_type` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notification_type` tinyint(4) NOT NULL DEFAULT 1 COMMENT '1 = toastr; 2 = sweetalert; 3 = notifyjs',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `company_name`, `site_title`, `site_title_bn`, `site_short_description`, `site_short_description_bn`, `site_header_logo`, `site_footer_logo`, `site_favicon`, `site_banner_image`, `site_email`, `site_phone_primary`, `site_phone_secondary`, `site_address`, `mail_driver`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `mail_encryption`, `facebook_url`, `twitter_url`, `google_plus_url`, `linkedin_url`, `youtube_url`, `instagram_url`, `pinterest_url`, `tumblr_url`, `flickr_url`, `recaptcha_key`, `recaptcha_secret`, `facebook_key`, `facebook_secret`, `twitter_key`, `twitter_secret`, `google_plus_key`, `google_plus_secret`, `google_map_api`, `image_width`, `image_height`, `image_size`, `file_type`, `notification_type`, `created_at`, `updated_at`) VALUES
(1, 'Best CNC Limited', 'PROFESSIONAL CNC ROUTER MACHINE MANUFACTURER IN BANGLADESH', 'প্রফেশনাল সিএনসি রুটার মেশিন ম্যানুফ্যাকচারার বাংলাদেশ', '<p><span style=\"color: rgb(102, 102, 102); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; text-align: center;\">Best CNC is a Bangladesh manufacturer of ATC CNC Router, Wood CNC Router, Woodworking Carving Machine, Wood Engraving Machine, CNC Laser Cutter, CNC Plasma Cutter. Our CNC Routers are specialized for materials such as Woods, Plastics, Aluminum, Copper, Stone. If there is a need of advices or more details, please come to us. Thanks for visiting.</span><br></p>', '<p><span style=\"color: rgb(102, 102, 102); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;; text-align: center;\">বেস্ট সিএনসি হ\'ল এটিসি সিএনসি রাউটার, উড সিএনসি রাউটার, উড ওয়ার্কিং কারভিং মেশিন, উড এনগ্রাভিং মেশিন, সিএনসি লেজার কাটার, সিএনসি প্লাজমা কাটারের একটি বাংলাদেশী প্রস্তুতকারক। আমাদের সিএনসি রাউটারগুলি উডস, প্লাস্টিক, অ্যালুমিনিয়াম, কপার, স্টোন জাতীয় উপকরণগুলির জন্য বিশেষীকরণযোগ্য। যদি পরামর্শ বা আরও বিশদ প্রয়োজন হয় তবে দয়া করে আমাদের কাছে আসুন। পরিদর্শনের জন্য ধন্যবাদ.</span><br></p>', '20190821_1566385367712.png', '20190821_1566385399772.png', '20190821_1566373763949.jpg', '20190821_1566373763367.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1920', '500', '500', 'jpeg|png|jpg|gif', 1, NULL, '2019-08-21 05:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_role_type_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `blc_name_id` int(11) DEFAULT NULL,
  `pin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_no` varchar(91) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role_type_id`, `role_id`, `blc_name_id`, `pin`, `name`, `username`, `email`, `email_verified_at`, `password`, `designation`, `mobile_no`, `image`, `remember_token`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 19, NULL, '5012', 'Administrtor', NULL, 'developer@gmail.com', NULL, '$2y$10$l4hCDZTEHZFR.H8sA6gJxOl4.fC/ydBcuL/8YvaXOHc0iaxgBGuCi', 'test designation', '017925441876', NULL, NULL, NULL, NULL, '2020-04-21 02:29:27'),
(38, 1, 21, NULL, '5014', 'Redwan Ahamad', NULL, 'redwan@nanoit.biz', NULL, '$2y$12$Z8u26U850rFZ7M8f8zYhE.ZINKH9FpO7cJmbN29X6Xp1ICnKcAgUe', 'test designation', '01732333239', NULL, 'UMeZxUa1OpJ3CvZvGK4qv2Z8WKJFMnV2DErqYp5BwfceGvF9Dhn08hWEy8Uj', NULL, '2019-11-12 05:46:56', '2020-04-21 02:29:45'),
(54, 2, 18, NULL, '501', 'asadullah', NULL, 'asadullahkpi@gmail.com', NULL, '$2y$10$lDBhlyIzhRXypoEtGGdm7ugTE9NjeC48gyo/Mn5.ynXcxKzfeGl8a', 'test designation', '017925441876', NULL, NULL, NULL, '2020-04-21 01:44:14', '2020-04-21 02:28:56'),
(55, 1, 19, 1, '851', 'salman', NULL, 'salam@gmail.com', NULL, '$2y$10$7Sezn38Loq8/f04w3jqHZeCN13w7ZhtF/PDfpHYityZJtT4dXWrLy', 'test designation3', '01432333229', NULL, NULL, NULL, '2020-04-21 03:23:24', '2020-04-21 03:27:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE IF NOT EXISTS `user_roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(21, 38, 19, '2019-11-12 05:46:56', '2020-01-01 16:00:44');

-- --------------------------------------------------------

--
-- Table structure for table `user_role_types`
--

DROP TABLE IF EXISTS `user_role_types`;
CREATE TABLE IF NOT EXISTS `user_role_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Head office,BLC,etc',
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_role_types`
--

INSERT INTO `user_role_types` (`id`, `name`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Head Office', 1, 1, 1, '2020-04-20 03:14:27', '2020-04-20 06:13:06'),
(2, 'BLC', 1, 1, 1, '2020-04-20 03:14:49', '2020-04-20 06:13:10');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
